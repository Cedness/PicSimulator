#include "simulator/Processor.h"
#include "simulator/Commands.h"
#include "controller/Controller.h"
#include <chrono>
#include <string>
#include <string_view>
#include <stdexcept>
#include <QApplication>


int main(int argc, char* argv[]) {
    std::vector<std::string_view> args(argc);
    for (int i = 0; i < argc; i++) {
        args.at(i) = argv[i];
    }
    if (args.size() >= 3 && args.at(2) == "no-gui") {
        Processor processor(Commands::get());
        std::string program(args.at(1));
        processor.getCommandMemory().writeFileToMemory(program);
        std::cout << "Program:\n";
        processor.getCommandMemory().print(std::cout);
        std::cout << "Starting execution\n";
        auto startTime = std::chrono::high_resolution_clock::now();
        for (uint64_t i = 0; i < 10 * 27000000; i++) {
            processor.getControlUnit().doCycle();
        }
        auto endTime = std::chrono::high_resolution_clock::now();
        std::cout << processor.getMemory().readWRegister();
        std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count();
        return 0;
    } else {
        QApplication application(argc, argv);
        Controller controller(args);
        return QApplication::exec();
    }
}

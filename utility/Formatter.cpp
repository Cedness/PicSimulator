//
// Created by ced on 13.04.22.
//

#include "Formatter.h"


F operator ""_f(const char* lit, size_t len) {
    F formatter;
    formatter << lit;
    return formatter;
}

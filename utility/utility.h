//
// Created by ced on 06.04.22.
//

#ifndef PICSIMULATOR_UTILITY_H
#define PICSIMULATOR_UTILITY_H


#include "cross_platform.h"
#include "Formatter.h"
#include <cstdint>
#include <stdexcept>
#include <sstream>


#if DEBUG_LEVEL
#define ENSURE_BIT_SIZE_STATIC(T, BIT) static_assert(8 * sizeof(T) <= BIT, "BIT must be smaller than bitsize of T");
#define ENSURE_BIT_SIZE(T, bit)\
if (8 * sizeof(T) <= bit)\
    throw std::invalid_argument("bit must be smaller than bitsize of T");
#else
#define ENSURE_BIT_SIZE_STATIC(T, BIT)
#define ENSURE_BIT_SIZE(T, bit)
#endif //DEBUG_LEVEL

#define DEBUG(msg)
#define DEBUG_VERBOSE(msg)
#define RELEASE_INFO(msg)
#define CONSOLE_OUTPUT true
#define PIC_DEBUGGER true

#define COUT(msg) std::cout << msg
#if CONSOLE_OUTPUT
#if DEBUG_LEVEL
#undef DEBUG
#define DEBUG(msg) COUT(msg)
#if DEBUG_LEVEL >= 2
#undef DEBUG_VERBOSE
#define DEBUG_VERBOSE(msg) COUT(msg)
#endif //DEBUG_LEVEL >= 2
#else
//#undef RELEASE_INFO
//#define RELEASE_INFO(msg) std::cout << msg
#undef PIC_DEBUGGER
#define PIC_DEBUGGER false
#endif //DEBUG_LEVEL
#endif //CONSOLE_OUTPUT

template<auto VALUE, uint8_t BIT>
inline constexpr bool getBit();
template<auto VALUE, uint8_t BIT, bool STATE>
inline constexpr decltype(VALUE) getSetBit();
template<auto VALUE, uint8_t BIT, typename T>
inline constexpr decltype(VALUE) getSetBitTo(bool state);

template<uint8_t BIT, typename T>
inline constexpr bool getBit(T value);
template<uint8_t BIT, bool STATE, typename T>
inline constexpr T getSetBit(T value);
template<uint8_t BIT, bool STATE, typename T>
inline constexpr void setBit(T& value);
template<uint8_t BIT, typename T>
inline constexpr T getSetBitTo(T value, bool state);
template<uint8_t BIT, typename T>
inline constexpr void setBitTo(T& value, bool state);

template<typename T>
inline constexpr bool getBit(T value, uint8_t bit);
template<bool STATE, typename T>
inline constexpr T getSetBit(T value, uint8_t bit);
template<bool STATE, typename T>
inline constexpr void setBit(T& value, uint8_t bit);
template<typename T>
inline constexpr T getSetBitTo(T value, uint8_t bit, bool state);
template<typename T>
inline constexpr void setBitTo(T& value, uint8_t bit, bool state);

template<uint8_t PIVOT, typename T>
inline constexpr T getMask();
template<uint8_t FROM, uint8_t TO, typename T>
inline constexpr T getMask();
template<uint8_t PIVOT, typename T>
inline constexpr void andMask(T& value);
template<uint8_t FROM, uint8_t TO, typename T>
inline constexpr void andMask(T& value);
template<uint8_t PIVOT, typename T>
inline constexpr T getAndMasked(T value);
template<uint8_t FROM, uint8_t TO, typename T>
inline constexpr T getAndMasked(T value);

template<typename T>
inline constexpr void andMask(T& value, uint8_t pivot);
template<typename T>
inline constexpr void andMask(T& value, uint8_t from, uint8_t to);
template<typename T>
inline constexpr T getAndMasked(T value, uint8_t pivot);
template<typename T>
inline constexpr T getAndMasked(T value, uint8_t from, uint8_t to);
template<typename T>
inline constexpr T getMask(uint8_t pivot);
template<typename T>
inline constexpr T getMask(uint8_t from, uint8_t to);
template<typename T>
inline std::string toHex(T decimal);
std::string toHex16(uint16_t decimal);
std::string toHex8(uint8_t decimal);
template<typename T>
void toDecimal(const std::string& binary, T& decimal);
uint16_t toDecimal(const std::string& binary);


template<auto VALUE, uint8_t BIT>
inline constexpr bool getBit() {
    ENSURE_BIT_SIZE_STATIC(decltype(VALUE), BIT)
    return VALUE >> BIT & 1;
}

template<auto VALUE, uint8_t BIT, bool STATE>
inline constexpr decltype(VALUE) getSetBit() {
    ENSURE_BIT_SIZE(decltype(VALUE), BIT)
    if constexpr (STATE) {
        return VALUE | 1 << BIT;
    } else {
        return VALUE & ~(1 << BIT);
    }
}

template<auto VALUE, uint8_t BIT, typename T>
inline constexpr decltype(VALUE) getSetBitTo(bool state) {
    return state ? getSetBit<VALUE, BIT, true>() : getSetBit<VALUE, BIT, false>();
}

template<uint8_t BIT, typename T>
constexpr bool getBit(T value) {
    ENSURE_BIT_SIZE(T, BIT)
    return value >> BIT & 1;
}

template<uint8_t BIT, bool STATE, typename T>
inline constexpr T getSetBit(T value) {
    ENSURE_BIT_SIZE(T, BIT)
    if constexpr (STATE) {
        return value | 1 << BIT;
    } else {
        return value & ~(1 << BIT);
    }
}

template<uint8_t BIT, bool STATE, typename T>
inline constexpr void setBit(T& value) {
    value = getSetBit<BIT, STATE>(value);
}

template<uint8_t BIT, typename T>
inline constexpr T getSetBitTo(T value, bool state) {
    return state ? getSetBit<BIT, true>(value) : getSetBit<BIT, false>(value);
}

template<uint8_t BIT, typename T>
inline constexpr void setBitTo(T& value, bool state) {
    value = getSetBitTo<BIT>(value, state);
}

template<typename T>
constexpr bool getBit(T value, uint8_t bit) {
    ENSURE_BIT_SIZE(T, bit)
    return value >> bit & 1;
}

template<bool STATE, typename T>
inline constexpr T getSetBit(T value, uint8_t bit) {
    ENSURE_BIT_SIZE(T, bit)
    if constexpr (STATE) {
        return value | 1 << bit;
    } else {
        return value & ~(1 << bit);
    }
}

template<bool STATE, typename T>
constexpr void setBit(T& value, uint8_t bit) {
    value = getSetBit<STATE>(value, bit);
}

template<typename T>
constexpr T getSetBitTo(T value, uint8_t bit, bool state) {
    return state ? getSetBit<true>(value, bit) : getSetBit<false>(value, bit);
}

template<typename T>
constexpr void setBitTo(T& value, uint8_t bit, bool state) {
    value = getSetBitTo(value, bit, state);
}

template<uint8_t PIVOT, typename T>
inline constexpr void andMask(T& value) {
    value = getAndMasked<PIVOT>(value);
}

template<uint8_t FROM, uint8_t TO, typename T>
inline constexpr void andMask(T& value) {
    value = getAndMasked<FROM, TO>(value);
}

template<uint8_t PIVOT, typename T>
constexpr T getAndMasked(T value) {
    return value & getMask<PIVOT, T>();
}

template<uint8_t FROM, uint8_t TO, typename T>
constexpr T getAndMasked(T value) {
    return (value & getMask<FROM, TO, T>()) >> FROM;
}

template<uint8_t PIVOT, typename T>
constexpr T getMask() {
    T mask = 0;
    setBit<PIVOT, true>(mask);
    return mask - 1;
}

template<uint8_t FROM, uint8_t TO, typename T>
constexpr T getMask() {
    return ~getMask<FROM, T>() & getMask<TO, T>();
}

template<typename T>
inline constexpr void andMask(T& value, uint8_t pivot) {
    value = getAndMasked(value, pivot);
}

template<typename T>
inline constexpr void andMask(T& value, uint8_t from, uint8_t to) {
    value = getAndMasked(value, from, to);
}

template<typename T>
constexpr T getAndMasked(T value, uint8_t pivot) {
    return value & getMask<T>(pivot);
}

template<typename T>
constexpr T getAndMasked(T value, uint8_t from, uint8_t to) {
    return (value & getMask<T>(from, to)) >> from;
}

template<typename T>
constexpr T getMask(uint8_t pivot) {
    T mask = 0;
    setBit<true>(mask, pivot);
    return mask - 1;
}

template<typename T>
constexpr T getMask(uint8_t from, uint8_t to) {
    return ~getMask<T>(from) & getMask<T>(to);
}

template<typename T>
std::string toHex(T decimal) {
    std::stringstream sstream;
    sstream << std::hex << decimal;
    return sstream.str();
}

template<typename T>
void toDecimal(const std::string& binary, T& decimal) {
    for (char c : binary) {
        decimal = (decimal << 1) + (c == '1' ? 1 : 0);
    }
}


#endif //PICSIMULATOR_UTILITY_H
// copy

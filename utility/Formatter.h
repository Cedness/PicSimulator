//
// Created by ced on 13.04.22.
//

#ifndef PICSIMULATOR_FORMATTER_H
#define PICSIMULATOR_FORMATTER_H


#include <sstream>


class F {
public:
    F() = default;
    //F(const F&) = delete;
    //F& operator=(const F&) = delete;

    template<typename T>
    F& operator<<(const T& value) {
        stream_ << value;
        return *this;
    }

    std::string str() const {
        return stream_.str();
    }

    operator std::string() const {
        return stream_.str();
    }

    enum ConvertToString {
        to_str
    };

    std::string operator>>(ConvertToString) {
        return stream_.str();
    }
private:
    std::stringstream stream_;
};


F operator""_f(const char* lit, size_t len);


#endif //PICSIMULATOR_FORMATTER_H

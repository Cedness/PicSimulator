//
// Created by ced on 06.04.22.
//

#include "utility.h"

const char* MAPPINGS = "0123456789abcdef";

template<size_t NIBBLE_COUNT, typename T>
std::string toHexX(T decimal) {
    std::string hex(NIBBLE_COUNT, ' ');
    for (size_t i = 0; i < NIBBLE_COUNT; i++) {
        hex[NIBBLE_COUNT - 1 - i] = MAPPINGS[decimal % 16];
        decimal /= 16;
    }
    return hex;
}

std::string toHex16(uint16_t decimal) {
    return toHexX<4>(decimal);
}


std::string toHex8(uint8_t decimal) {
    return toHexX<2>(decimal);
}

uint16_t toDecimal(const std::string& binary) {
    uint16_t decimal;
    toDecimal(binary, decimal);
    return decimal;
}

//
// Created by ced on 05.04.22.
//

#include "Memory.h"
#include "../utility/utility.h"


Memory::Memory(Processor& processor)
        : processor_(processor) {
    // Map memory pointers
    for (uint8_t bank = 0; bank < 2; bank++) {
        const uint8_t memPtrOffset = bank * BANK_SIZE;
        const uint8_t memOffset = bank * USABLE_BANK_SIZE;
        for (uint8_t i = 0; i < USABLE_BANK_SIZE; i++) {
            memoryPointers_.at(memPtrOffset + i) = &memory_.at(memOffset + i);
        }
    }

    // Disable unimplemented memory
    for (uint8_t bank = 0; bank < 2; bank++) {
        const uint8_t memPtrOffset = bank * BANK_SIZE;
        for (uint8_t i = USABLE_BANK_SIZE; i < BANK_SIZE; i++) {
            memoryPointers_.at(memPtrOffset + i) = &unimplementedMemory_;
        }
    }
    memoryPointers_.at(AN_UNIMPLEMENTED_LOCATION) = &unimplementedMemory_;
    memoryPointers_.at(EECON2) = &unimplementedMemory_;

    // Pair memory which is shared between banks
    setEqual<INDIRECT_ADDR>();
    setEqual<PCL>();
    setEqual<STATUS>();
    setEqual<FSR>();
    setEqual<AN_UNIMPLEMENTED_LOCATION>();
    setEqual<PCLATH>();
    setEqual<INTCON>();

    initialize();
}

void Memory::powerReset() {
    wRegister_ = 0;
    memory_.fill(0);
    initialize();
}

void Memory::softReset() {
    direct(PCL) = CommandMemory::START_ADDRESS;
    direct(STATUS) &= getMask<STATUS_BITS::RP0, uint8_t>();
    direct(PCLATH) = 0;
    direct(INTCON) &= ~getMask<INTCON_BITS::INTF, uint8_t>();
    direct(OPTION) = -1;
    direct(TRISA) = getMask<uint8_t>(RA4_T0CKI + 1);
    direct(TRISB) = -1;
    uint8_t& eecon1 = direct(EECON1);
    if (getBit<EECON1_BITS::WR>(eecon1)) {
        eecon1 = getSetBit<0, EECON1_BITS::WRERR, true>();
    } else {
        eecon1 = 0;
    }

}

uint8_t Memory::readWRegister() {
    return wRegister_;
}

uint8_t& Memory::direct(uint8_t address) {
    return *memoryPointers_.at(address);
}

uint8_t Memory::direct(uint8_t address) const {
    return *memoryPointers_.at(address);
}

uint8_t Memory::get(uint8_t address) const {
    calcIndirectAddress(address);
    return *memoryPointers_.at(address);
}

void Memory::set(uint8_t address, uint8_t value) {
    calcIndirectAddress(address);
    writeInternal<false>(address, value);
}

uint8_t Memory::read(uint8_t address) {
    calcBankAddress(address);
    calcIndirectAddress(address);
    switch (address) {
        case PORTA:
            return processor_.getIOPorts().port<IOPorts::A>().read();
        case PORTB:
            return processor_.getIOPorts().port<IOPorts::B>().read();
        default:
            return *memoryPointers_.at(address);
    }
}

void Memory::updateZeroFlag(uint8_t value) {
    setBitTo<ZERO>(direct(STATUS), value == 0);
}

void Memory::updateCarryFlag(bool carry) {
    setBitTo<CARRY>(direct(STATUS), carry);
}

void Memory::updateDigitCarryFlag(bool digitCarry) {
    setBitTo<DIGIT_CARRY>(direct(STATUS), digitCarry);
}

void Memory::initialize() {
    direct(PCL) = CommandMemory::START_ADDRESS;
    setBit<true>(direct(STATUS), POWER_DOWN);
    setBit<true>(direct(STATUS), TIME_OUT);
    direct(OPTION) = -1;
    direct(TRISA) = getMask<uint8_t>(RA4_T0CKI + 1);
    direct(TRISB) = -1;
}

uint8_t Memory::getBank() {
    return getBit<RP0>(memory_.at(STATUS));
}

void Memory::calcBankAddress(uint8_t& address) {
    bool bank = getBank();
    setBitTo<BANK_ADDRESS_BIT>(address, bank);
}

void Memory::calcIndirectAddress(uint8_t& address) const {
    if (address == INDIRECT_ADDR || address == getSetBit<INDIRECT_ADDR, BANK_ADDRESS_BIT, true>()) {
        // use value in FSR as address for indirect addressing
        address = direct(FSR);
    }
}

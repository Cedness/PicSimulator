//
// Created by ced on 02.05.22.
//

#include "Prescaler.h"


Prescaler::Prescaler(Processor& processor)
        : processor_(processor) {}

void Prescaler::powerReset() {
    prescaler_ = 0;
}

uint8_t Prescaler::doCycle() {
    if (prescaler_) {
        prescaler_--;
    } else {
        uint8_t option = processor_.getMemory().direct(Memory::OPTION);
        uint8_t prescalerBase = getBit<Memory::OPTION_BITS::PSA>(option) ? WatchdogTimer::PRESCALER_BASE : Timer0::PRESCALER_BASE;
        prescaler_ = (prescalerBase << getAndMasked<Memory::OPTION_BITS::PS2 + 1>(option)) - 1;
    }
    return prescaler_;
}

uint8_t Prescaler::get() {
    return prescaler_;
}

void Prescaler::clear() {
    prescaler_ = 0;
}

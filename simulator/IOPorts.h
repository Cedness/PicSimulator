//
// Created by Ced on 01/05/2022.
//

#ifndef PICSIMULATOR_PROCESSOR_H_DEF
#ifndef PICSIMULATOR_IOPORTS_H_DEC
#define PICSIMULATOR_IOPORTS_H_DEC


#include <cstdint>
#include <array>


class Processor;

class IOPorts {
public:
    enum PortID {
        A = 0, B
    };
    template<PortID ID>
    class IOPort {
    public:
        IOPort(IOPorts& ioPorts, uint8_t portAddress, uint8_t trisAddress);
        void powerReset();
        uint8_t get() const;
        void set(uint8_t value);
        uint8_t read();
        void write(uint8_t value);
        void writeTris(uint8_t tris);
        void checkPortBInterrupt(uint8_t& intcon);
    private:
        IOPorts& ioPorts_;
        const uint8_t portAddress_;
        const uint8_t trisAddress_;
        uint8_t latch_ = 0;

        uint8_t& accessPort();
        uint8_t getTris();
        void putLatchOnPort(uint8_t tris);
        void putOnPort(uint8_t value, uint8_t mask);
    };
    explicit IOPorts(Processor& processor);
    void powerReset();
    template<PortID ID>
    constexpr IOPort<ID>& port();
    template<PortID ID>
    constexpr const IOPort<ID>& port() const;
private:
    constexpr static uint8_t PORTA_T0CKI_BIT = 4;
    constexpr static uint8_t PORTB_INTERRUPT_BIT = 0;
    constexpr static uint8_t PORTB_INTERRUPT_FROM = 4;

    Processor& processor_;
    IOPort<A> portA_;
    IOPort<B> portB_;
};


#include "Processor.h"


#endif //PICSIMULATOR_IOPORTS_H_DEC
#endif //PICSIMULATOR_PROCESSOR_H_DEF

#ifdef PICSIMULATOR_PROCESSOR_H_DEF
#ifndef PICSIMULATOR_IOPORTS_H_DEF
#define PICSIMULATOR_IOPORTS_H_DEF


template<IOPorts::PortID ID>
IOPorts::IOPort<ID>::IOPort(IOPorts& ioPorts, uint8_t portAddress, uint8_t trisAddress)
        : ioPorts_(ioPorts), portAddress_(portAddress), trisAddress_(trisAddress) {}

template<IOPorts::PortID ID>
void IOPorts::IOPort<ID>::powerReset() {
    latch_ = 0;
}

template<IOPorts::PortID ID>
uint8_t IOPorts::IOPort<ID>::get() const {
    return accessPort();
}

template<IOPorts::PortID ID>
void IOPorts::IOPort<ID>::set(uint8_t value) {
    uint8_t tris = getTris();
    putOnPort(value, tris);
    if constexpr (ID == B) {
        checkPortBInterrupt(ioPorts_.processor_.getMemory().direct(Memory::INTCON));
    }
}

template<IOPorts::PortID ID>
uint8_t IOPorts::IOPort<ID>::read() {
    latch_ = accessPort();
    return latch_;
}

template<IOPorts::PortID ID>
void IOPorts::IOPort<ID>::write(uint8_t value) {
    latch_ = value;
    putLatchOnPort(getTris());
}

template<IOPorts::PortID ID>
void IOPorts::IOPort<ID>::writeTris(uint8_t tris) {
    putLatchOnPort(tris);
}

template<IOPorts::PortID ID>
void IOPorts::IOPort<ID>::checkPortBInterrupt(uint8_t& intcon) {
    static_assert(ID == B);
    uint8_t mask = ~getMask<PORTB_INTERRUPT_FROM, uint8_t>() & getTris();
    if ((accessPort() & mask) != (latch_ & mask)) {
        setBit<Memory::INTCON_BITS::RBIF, true>(intcon);
    }
}

template<IOPorts::PortID ID>
uint8_t& IOPorts::IOPort<ID>::accessPort() {
    return ioPorts_.processor_.getMemory().direct(portAddress_);
}

template<IOPorts::PortID ID>
uint8_t IOPorts::IOPort<ID>::getTris() {
    return ioPorts_.processor_.getMemory().direct(trisAddress_);
}

template<IOPorts::PortID ID>
void IOPorts::IOPort<ID>::putLatchOnPort(uint8_t tris) {
    putOnPort(latch_, ~tris);
}

template<IOPorts::PortID ID>
void IOPorts::IOPort<ID>::putOnPort(uint8_t value, uint8_t mask) {
    uint8_t& port = accessPort();
    if constexpr (ID == A) {
        bool risingEdge;
        if (getBit<PORTA_T0CKI_BIT>(mask) && (risingEdge = getBit<PORTA_T0CKI_BIT>(value)) != getBit<PORTA_T0CKI_BIT>(port)) {
            ioPorts_.processor_.getTimer0().onT0CKI(risingEdge);
        }
    }
    if constexpr (ID == B) {
        bool risingEdge;
        if (getBit<PORTB_INTERRUPT_BIT>(mask) && (risingEdge = getBit<PORTB_INTERRUPT_BIT>(value)) != getBit<PORTB_INTERRUPT_BIT>(port)) {
            auto& memory = ioPorts_.processor_.getMemory();
            bool risingEdgeRequired = getBit<Memory::OPTION_BITS::INTEDG>(memory.direct(Memory::OPTION));
            if (risingEdge == risingEdgeRequired) {
                setBit<Memory::INTCON_BITS::INTF, true>(memory.direct(Memory::INTCON));
            }
        }
    }
    port &= ~mask;
    port |= value & mask;
}

template<IOPorts::PortID ID>
constexpr IOPorts::IOPort<ID>& IOPorts::port() {
    if constexpr (ID == A) {
        return portA_;
    } else {
        return portB_;
    }
}

template<IOPorts::PortID ID>
constexpr const IOPorts::IOPort<ID>& IOPorts::port() const {
    if constexpr (ID == A) {
        return portA_;
    } else {
        return portB_;
    }
}


#endif //PICSIMULATOR_IOPORTS_H_DEF
#endif //PICSIMULATOR_PROCESSOR_H_DEF

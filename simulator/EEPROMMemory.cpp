//
// Created by ced on 10.05.22.
//

#include "EEPROMMemory.h"


EEPROMMemory::EEPROMMemory(Processor& processor)
        : processor_(processor) {}

void EEPROMMemory::powerReset() {
    memory_.fill(0);
    doneAt_ = std::chrono::nanoseconds(0);
    securityStage = 0;
}

void EEPROMMemory::doCycle() {
    if (doneAt_ == std::chrono::nanoseconds(0)) {
        return;
    }
    if (doneAt_ > processor_.getControlUnit().getTime()) {
        return;
    }
    doneAt_ = std::chrono::nanoseconds(0);
    uint8_t& eecon1 = processor_.getMemory().direct(Memory::EECON1);
    if (getBit<Memory::EECON1_BITS::RD>(eecon1)) {
        setBit<Memory::EECON1_BITS::RD, false>(eecon1);
    }
    if (getBit<Memory::EECON1_BITS::WR>(eecon1)) {
        setBit<Memory::EECON1_BITS::WR, false>(eecon1);
        setBit<Memory::EECON1_BITS::EEIF, true>(eecon1);
    }
}

uint8_t EEPROMMemory::read(uint8_t address) {
    if (address >= MEMORY_SIZE) {
        return 0;
    }
    return memory_.at(address);
}

void EEPROMMemory::write(uint8_t address, uint8_t value) {
    if (address >= MEMORY_SIZE) {
        return;
    }
    memory_.at(address) = value;
}

void EEPROMMemory::eecon1Write(uint8_t value) {
    processRequest(value);
    processor_.getMemory().direct(Memory::EECON1) = value;
}

void EEPROMMemory::eecon2Write(uint8_t value) {
    if (securityUnlocked()) {
        return;
    }
    if (value == SECURITY_NUMBERS.at(securityStage)) {
        securityStage++;
    } else {
        securityStage = 0;
    }
}

bool EEPROMMemory::securityUnlocked() {
    return securityStage >= SECURITY_STAGE_COUNT;
}

void EEPROMMemory::processRequest(uint8_t& value) {
    uint8_t eecon1 = processor_.getMemory().direct(Memory::EECON1);
    if (getBit<Memory::EECON1_BITS::RD>(eecon1) || getBit<Memory::EECON1_BITS::WR>(eecon1)) {
        setBitTo<Memory::EECON1_BITS::RD>(value, getBit<Memory::EECON1_BITS::RD>(eecon1));
        setBitTo<Memory::EECON1_BITS::WR>(value, getBit<Memory::EECON1_BITS::WR>(eecon1));
        return;
    }
    bool readRequest = getBit<Memory::EECON1_BITS::RD>(value);
    if (readRequest) {
        doRead();
    }
    if (getBit<Memory::EECON1_BITS::WR>(value)) {
        if (readRequest || !getBit<Memory::EECON1_BITS::WREN>(eecon1) || !securityUnlocked()) {
            setBit<Memory::EECON1_BITS::WR, false>(value);
            return;
        }
        doWrite();
    }
}

void EEPROMMemory::doRead() {
    auto& memory = processor_.getMemory();
    memory.direct(Memory::EEDATA) = read(memory.direct(Memory::EEADR));
    doneAt_ = processor_.getControlUnit().getTime() + READ_DELAY;
}

void EEPROMMemory::doWrite() {
    auto& memory = processor_.getMemory();
    write(memory.direct(Memory::EEADR), memory.direct(Memory::EEDATA));
    securityStage = 0;
    doneAt_ = processor_.getControlUnit().getTime() + WRITE_DELAY;
}

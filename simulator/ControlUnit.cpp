//
// Created by ced on 12.04.22.
//

#include "ControlUnit.h"
#include "Commands.h"
#include "../utility/utility.h"
#include <iostream>


ControlUnit::ControlUnit(Processor& processor)
        : processor_(processor) {
    setFrequency(DEFAULT_FREQUENCY);
}

void ControlUnit::powerOnReset() {
    processor_.getMemory().powerReset();
    processor_.getIOPorts().powerReset();
    processor_.getStack().powerReset();
    processor_.getEEPROMMemory().powerReset();
    processor_.getTimer0().powerReset();
    processor_.getWatchdogTimer().powerReset();
    processor_.getPrescaler().powerReset();
    processor_.getCommandMemory().powerReset();
    nextCommand_ = processor_.getCommandMemory().read(CommandMemory::START_ADDRESS);
    sleeping_ = false;
    cycle_ = 0;
    time_ = std::chrono::nanoseconds(0);
    setFrequency(DEFAULT_FREQUENCY);
}

void ControlUnit::softReset() {
    processor_.getMemory().softReset();
    processor_.getCommandMemory().softReset();
    nextCommand_ = processor_.getCommandMemory().read(CommandMemory::START_ADDRESS);
}

void ControlUnit::doCycle() {
    if (breaking_) {
        breaking_ = false;
    } else {
        processor_.getWatchdogTimer().doCycle();
        if (!sleeping_) {
            processor_.getTimer0().doCycle();
        }
        processor_.getEEPROMMemory().doCycle();
        doInterrupts();

        auto& commandMemory = processor_.getCommandMemory();
        if (commandMemory.isBreakpoint(commandMemory.getProgramCounter()) && nextCommand_ == commandMemory.read(commandMemory.getProgramCounter())) {
            breaking_ = true;
            return;
        }
    }
    if (!sleeping_) {
        runCommand();
        cycle_++;

#if PIC_DEBUGGER
        auto print = [](uint16_t value) {
            DEBUG(toHex8(value) << "h[" << value << "]");
        };

        auto printB = [](bool value) {
            DEBUG((value ? "1" : "0"));
        };

        auto& memory = processor_.getMemory();
        uint8_t w = memory.readWRegister();
        uint8_t status = memory.direct(Memory::STATUS);
        DEBUG(" | DC=");
        printB(getBit<Memory::DIGIT_CARRY>(status));
        DEBUG(" C=");
        printB(getBit<Memory::CARRY>(status));
        DEBUG(" Z=");
        printB(getBit<Memory::ZERO>(status));
        DEBUG(" W=");
        print(w);
        DEBUG(" @0ch=");
        print(memory.direct(0x0c));
        DEBUG(" @0dh=");
        print(memory.direct(0x0d));
        DEBUG(" @0eh=");
        print(memory.direct(0x0e));
        DEBUG(" @010=");
        print(memory.direct(0x10));
#endif //PIC_DEBUGGER
        DEBUG("\n");
    }
    time_ += instructionTime_;
}

uint16_t ControlUnit::getNextCommand() {
    return nextCommand_;
}

void ControlUnit::setNextCommand(uint16_t nextCommand) {
    nextCommand_ = nextCommand;
}

bool ControlUnit::isSleeping() {
    return sleeping_;
}

bool ControlUnit::isBreaking() {
    return breaking_;
}

void ControlUnit::putToSleep() {
    sleeping_ = true;
}

void ControlUnit::wakeUp() {
    sleeping_ = false;
}

int64_t ControlUnit::getFrequency() {
    return instructionsPerSecond_ * CYCLES_PER_INSTRUCTION;
}

void ControlUnit::setFrequency(int64_t frequency) {
    setInstructionsPerSecond(frequency / CYCLES_PER_INSTRUCTION);
}

void ControlUnit::setInstructionsPerSecond(int64_t instructionsPerSecond) {
    if (instructionsPerSecond < 1) {
        instructionsPerSecond = 1;
    }
    instructionsPerSecond_ = instructionsPerSecond;
    instructionTime_ = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::seconds(1)) / instructionsPerSecond_;
}

std::chrono::nanoseconds ControlUnit::getInstructionTime() {
    return instructionTime_;
}

uint64_t ControlUnit::getCycle() {
    return cycle_;
}

std::chrono::nanoseconds ControlUnit::getTime() {
    return time_;
}

void ControlUnit::doInterrupts() {
    auto& memory = processor_.getMemory();
    uint8_t& intcon = memory.direct(Memory::INTCON);
    //Check for interrupt
    if (!(getBit<Memory::INTCON_BITS::T0IE>(intcon) && getBit<Memory::INTCON_BITS::T0IF>(intcon) && !sleeping_) &&
        !(getBit<Memory::INTCON_BITS::INTE>(intcon) && getBit<Memory::INTCON_BITS::INTF>(intcon)) &&
        !(getBit<Memory::INTCON_BITS::RBIE>(intcon) && getBit<Memory::INTCON_BITS::RBIF>(intcon)) &&
        !(getBit<Memory::INTCON_BITS::EEIE>(intcon) && getBit<Memory::EECON1_BITS::EEIF>(memory.direct(Memory::EECON1)))) {
        return;
    }
    //Interrupt detected
    if (sleeping_) {
        sleeping_ = false;
        return;
    }
    if (!getBit<Memory::INTCON_BITS::GIE>(intcon)) {
        return;
    }
    //Disable interrupts
    setBit<Memory::INTCON_BITS::GIE, false>(intcon);

    auto& commandMemory = processor_.getCommandMemory();
    //Save PC
    uint16_t programCounter = commandMemory.getProgramCounter();
    if (nextCommand_ != commandMemory.read(programCounter)) {
        //artificial nop is queued, skip it by incrementing the return address
        commandMemory.setProgramCounter(programCounter + 1);
    }
    processor_.getStack().pushToStack(commandMemory.getProgramCounter());
    //Start ISR
    commandMemory.setProgramCounter(CommandMemory::ISR_ADDRESS);
    nextCommand_ = commandMemory.read(commandMemory.getProgramCounter());
}

void ControlUnit::runCommand() {
    auto& commandMemory = processor_.getCommandMemory();

    auto command = nextCommand_;
    nextCommand_ = commandMemory.read(commandMemory.getProgramCounter() + 1);

    auto& commands = processor_.getCommands();
    auto& commandData = processor_.getCommands().getCommandData(command);
    RELEASE_INFO(cycle_ << "\n");
    DEBUG(cycle_ << " | " << toHex16(commandMemory.getProgramCounter()) << " " << toHex16(command));
    DEBUG(" | " << commands.toString(command, commandData));
    DEBUG(std::flush);

    //Execute command
    commandData.microcode(&processor_.getCommands(), command, commandData, processor_);

    commandMemory.setProgramCounter(commandMemory.getProgramCounter() + 1);
}

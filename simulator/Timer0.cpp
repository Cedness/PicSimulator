//
// Created by Ced on 01/05/2022.
//

#include "Timer0.h"

Timer0::Timer0(Processor& processor)
        : processor_(processor) {}

void Timer0::powerReset() {
    inhibitCycles_ = 0;
}

void Timer0::doCycle() {
    if (inhibitCycles_) {
        inhibitCycles_--;
        return;
    }
    auto& memory = processor_.getMemory();
    uint8_t option = memory.direct(Memory::OPTION);
    if (getBit<Memory::OPTION_BITS::T0CS>(option)) {
        return;
    }
    count(option);
}

void Timer0::onT0CKI(bool risingEdge) {
    if (inhibitCycles_) {
        return;
    }
    auto& memory = processor_.getMemory();
    uint8_t option = memory.direct(Memory::OPTION);
    if (!getBit<Memory::OPTION_BITS::T0CS>(option) || getBit<Memory::OPTION_BITS::T0SE>(option) == risingEdge) {
        return;
    }
    count(option);
}

void Timer0::setWritten() {
    inhibitCycles_ = INHIBIT_CYCLES_AFTER_WRITE;
    if (!getBit<Memory::OPTION_BITS::PSA>(processor_.getMemory().direct(Memory::OPTION))) {
        processor_.getPrescaler().clear();
    }
}

void Timer0::count(uint8_t option) {
    auto& memory = processor_.getMemory();
    if (!getBit<Memory::OPTION_BITS::PSA>(option)) {
        auto& prescaler = processor_.getPrescaler();
        prescaler.doCycle();
        if (prescaler.get()) {
            return;
        }
    }
    uint8_t timer0 = ++memory.direct(Memory::TMR0);
    if (timer0 == 0) {
        //Set interrupt flag
        setBit<Memory::INTCON_BITS::T0IF, true>(memory.direct(Memory::INTCON));
    }
}

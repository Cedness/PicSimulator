//
// Created by ced on 02.05.22.
//

#include "WatchdogTimer.h"


WatchdogTimer::WatchdogTimer(Processor& processor)
        : processor_(processor) {}

void WatchdogTimer::powerReset() {
    timeout_ = TIMEOUT_PERIOD;
}

void WatchdogTimer::doCycle() {
    if (!getBit<CommandMemory::CONFIG_BITS::WDTE>(processor_.getCommandMemory().getConfig())) {
        return;
    }
    if (timeout_ >= processor_.getControlUnit().getTime()) {
        return;
    }
    timeout_ += TIMEOUT_PERIOD;
    auto& memory = processor_.getMemory();
    if (getBit<Memory::OPTION_BITS::PSA>(memory.direct(Memory::OPTION)) && processor_.getPrescaler().doCycle()) {
        return;
    }
    setBit<Memory::STATUS_BITS::TIME_OUT, false>(memory.direct(Memory::STATUS));
    auto& controlUnit = processor_.getControlUnit();
    if (controlUnit.isSleeping()) {
        controlUnit.wakeUp();
    } else {
        controlUnit.softReset();
    }
}

void WatchdogTimer::clear() {
    auto& memory = processor_.getMemory();
    uint8_t option = memory.direct(Memory::OPTION);
    if (getBit<Memory::OPTION_BITS::PSA>(option)) {
        processor_.getPrescaler().clear();
    }
    timeout_ = processor_.getControlUnit().getTime() + TIMEOUT_PERIOD;
}

int64_t WatchdogTimer::get() {
    if (getBit<CommandMemory::CONFIG_BITS::WDTE>(processor_.getCommandMemory().getConfig())) {
        auto& controlUnit = processor_.getControlUnit();
        return (timeout_ - controlUnit.getTime()) / controlUnit.getInstructionTime();
    } else {
        return 0;
    }
}

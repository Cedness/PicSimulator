//
// Created by ced on 05.04.22.
//

#include "Commands.h"
#include "../except/CommandNotImplementedError.h"
#include <functional>
#include <utility>


const std::string Commands::CommandData::ALLOWED_CHARS = { ZERO, ONE, REDUNDANT, DESTINATION, FILE, LITERAL, BIT };
std::weak_ptr<Commands> Commands::INSTANCE = std::weak_ptr<Commands>();

Commands::CommandData::CommandData(Commands* commands, std::string&& name_, std::string&& binary_, std::function<void(CARGS_FULL)> microcode_)
        : name(std::move(name_)), code(0), binary(std::move(binary_)), microcode(std::move(microcode_)) {
    //Check binary size
    if (binary.size() != COMMAND_BITS)
        throw std::invalid_argument(F() << "Binary string has length " << (int) binary.size() << " instead of " << (int) COMMAND_BITS << ": " << binary);

    //Check illegal chars
    for (char c : binary) {
        if (ALLOWED_CHARS.find(c) == std::string::npos)
            throw std::invalid_argument(F() << "Binary string contains illegal characters: " << c << " in " << binary);
    }

    //Convert binary to dec and hex
    toDecimal(binary, code);
    hex = toHex16(code);

    //Calc properties
    if (check<0, FILE>()) {
        hasAddress = true;
        if (check<CMD_DESTINATION_BIT, DESTINATION>()) {
            hasDestination = true;
        } else if (check<CMD_BIT_TO - 1, BIT>()) {
            hasBits = true;
        }
    } else if (check<CMD_LABEL_BITS - 1, LITERAL>()) {
        hasLabel = true;
    } else if (check<0, LITERAL>()) {
        hasLiteral = true;
    }

    /*
     * Add to index
     * Assumption is made that if there is a variable bit in the indexed bits range, there is no less significat non-variable bit in the range.
     */
    uint16_t keyRange = INDEX_SIZE;
    for (char c : binary) {
        if (keyRange <= 1 || (c != ZERO && c != ONE))
            break;
        keyRange /= 2;
    }
    auto& commandsData = commands->commandsData_;
    uint16_t baseKey = code >> VARIABLE_BITS;
    uint16_t nextKey = baseKey + keyRange;
    for (uint16_t key = baseKey; key < nextKey; key++) {
        auto& keyedCommandsData = commandsData.at(key);
        if (!keyedCommandsData.empty()) {
            auto checkList = keyedCommandsData; //this is a copy
            for (uint8_t j = INDEXED_BITS; j < INDEXED_BITS + VARIABLE_BITS; j++) {
                char c = binary.at(j);
                if (c != ONE && c != ZERO)
                    continue;
                bool verified = false;
                for (auto it = checkList.begin(); it != checkList.end();) {
                    auto* commandData = *it;
                    if (commandData == nullptr)
                        throw std::logic_error("nullptr in commandsData, this should not happen");
                    char otherC = commandData->binary.at(j);
                    if ((otherC == ONE || otherC == ZERO) && c != otherC) {
                        it = checkList.erase(it);
                        if (checkList.empty()) {
                            verified = true;
                            break;
                        }
                    } else {
                        it++;
                    }
                }
                if (verified)
                    break;
            }
            if (!checkList.empty())
                throw std::logic_error(F() << "Multiple commands have the same code: " << name << " and " << checkList.front()->name);
        }
        keyedCommandsData.push_back(this);
    }
}

std::stringstream& operator<<(std::stringstream& out, const Commands::CommandData& commandData) {
    out << commandData.hex << "|" << commandData.name;
    return out;
}

std::ostream& operator<<(std::ostream& out, const Commands::CommandData& commandData) {
    out << commandData.hex << "|" << commandData.name;
    return out;
}

std::string Commands::CommandData::str() {
    std::stringstream stream;
    stream << *this;
    return stream.str();
}

Commands::CommandData::operator Command() const {
    return code;
}

template<uint8_t BIT, char SYMBOL>
bool Commands::CommandData::check() {
    return binary.at(COMMAND_BITS - BIT - 1) == SYMBOL;
}

std::shared_ptr<Commands> Commands::get() {
    if (std::shared_ptr<Commands> commands = INSTANCE.lock())
        return commands;
    std::shared_ptr<Commands> commands = std::make_shared<Commands>();
    INSTANCE = commands;
    return commands;
}

Commands::Commands() {
    if (!INSTANCE.expired())
        throw std::runtime_error("AAAAAAAAlready instantiated ALAAAAAAAARM");
}

#define STRICT_COMMAND_CHECK false

const Commands::CommandData& Commands::getCommandData(Command command) {
    uint8_t key = command >> VARIABLE_BITS;
    auto& candidates = commandsData_.at(key); //this is a copy

    // No candidate, invalid command
    if (candidates.empty())
        throw CommandNotImplementedError(command);
#if !STRICT_COMMAND_CHECK
    /**
     * Only one candidate, return it.
     * In this case, the not indexed bits are disregarded.
     */
    if (candidates.size() == 1)
        return *candidates.front();
#endif //!STRICT_COMMAND_CHECK

    /*
     * Find correct candidate if there are multiple commands sharing the same indexed bits.
     * This is performed lazily. If one command matches i.e. 3 further bits and the other candidates do not, this one is selected and the remaining bits are disregarded.
     */
    auto checkList = candidates; //this is a copy
    for (uint8_t i = 0; i < VARIABLE_BITS; i++) {
        char wanted = getBit(command, VARIABLE_BITS - i - 1) ? CommandData::ONE : CommandData::ZERO;
        for (auto it = checkList.begin(); it != checkList.end();) {
            char found = (*it)->binary[INDEXED_BITS + i];
            if (found == wanted || (found != CommandData::ZERO && found != CommandData::ONE)) {
                it++;
            } else {
                it = checkList.erase(it);
#if STRICT_COMMAND_CHECK
                // No candidate left, invalid command
                if (checkList.empty())
                    throw CommandNotImplementedError(command);
#else
                if (checkList.size() == 1)
                    return *checkList.front();
#endif //STRICT_COMMAND_CHECK
            }
        }
    }
#if STRICT_COMMAND_CHECK
    if (checkList.size() != 1)
        throw std::logic_error(F() << "Multiple commands have the same code, this should not happen: " << command);
    return *checkList.front();
#else
    throw std::logic_error(F() << "Multiple commands have the same code, this should not happen: " << command);
#endif //STRICT_COMMAND_CHECK
}

uint8_t Commands::getLiteral(Command command) {
    return getAndMasked(command, CMD_LITERAL_BITS);
}

uint8_t Commands::getAddress(Command command) {
    return getAndMasked(command, CMD_ADDRESS_BITS);
}

uint16_t Commands::getLabel(Command command) {
    return getAndMasked(command, CMD_LABEL_BITS);
}

uint8_t Commands::getBitIndex(Command command) {
    return getAndMasked(command, CMD_BIT_FROM, CMD_BIT_TO);
}

bool Commands::getDestination(Command command) {
    return getBit<CMD_DESTINATION_BIT>(command);
}

std::string Commands::toString(Command command) {
    try {
        return toString(command, getCommandData(command));
    } catch (CommandNotImplementedError&) {
        return "Unimplemented command";
    }
}

std::string Commands::toString(uint16_t command, const CommandData& commandData) {
    std::stringstream sstream;
    sstream << commandData.name;
    if (commandData.hasAddress) {
        sstream << " " << toHex8(getAddress(command)) << "h";
        if (commandData.hasDestination && !getDestination(command)) {
            sstream << ",w";
        } else if (commandData.hasBits) {
            sstream << "," << static_cast<uint16_t>(getBitIndex(command));
        }
    } else if (commandData.hasLiteral) {
        sstream << " " << toHex8(getLiteral(command)) << "h";
    } else if (commandData.hasLabel) {
        sstream << " " << toHex16(getLabel(command)) << "h";
    }
    return sstream.str();
}

void Commands::addwf(CARGS) {
    auto& memory = processor.getMemory();
    uint8_t address = getAddress(command);
    auto[result, carry, digitCarry] = arithmetic<ArithType::ADD>(CA_PASS, memory.readWRegister(), memory.read(address));
    storeAtDestination(CA_PASS, address, result);
    updateFlags(CA_PASS, carry, digitCarry);
}

void Commands::andwf(CARGS) {
    auto& memory = processor.getMemory();
    uint8_t address = getAddress(command);
    uint8_t result = memory.readWRegister() & memory.read(address);
    storeAtDestination(CA_PASS, address, result);
}

void Commands::clrf(CARGS) {
    processor.getMemory().write<true>(getAddress(command), 0);
}

void Commands::clrw(CARGS) {
    processor.getMemory().writeWRegister<true>(0);
}

void Commands::comf(CARGS) {
    uint8_t address = getAddress(command);
    uint8_t result = ~processor.getMemory().read(address);
    storeAtDestination(CA_PASS, address, result);
}

void Commands::decf(CARGS) {
    crement<CrementType::DEC>(CA_PASS);
}

void Commands::decfsz(CARGS) {
    if (!crement<CrementType::DEC, false>(CA_PASS)) {
        skip(CA_PASS);
    }
}

void Commands::incf(CARGS) {
    crement<CrementType::INC>(CA_PASS);
}

void Commands::incfsz(CARGS) {
    if (!crement<CrementType::INC, false>(CA_PASS)) {
        skip(CA_PASS);
    }
}

void Commands::iorwf(CARGS) {
    auto& memory = processor.getMemory();
    uint8_t address = getAddress(command);
    uint8_t result = memory.readWRegister() | memory.read(address);
    storeAtDestination(CA_PASS, address, result);
}

void Commands::movf(CARGS) {
    uint8_t address = getAddress(command);
    uint8_t result = processor.getMemory().read(address);
    storeAtDestination(CA_PASS, address, result);
}

void Commands::movwf(CARGS) {
    auto& memory = processor.getMemory();
    memory.write(getAddress(command), memory.readWRegister());
}

void Commands::nop(CARGS) {}

void Commands::rlf(CARGS) {
    rotate<RotationType::LEFT>(CA_PASS);
}

void Commands::rrf(CARGS) {
    rotate<RotationType::RIGHT>(CA_PASS);
}

void Commands::subwf(CARGS) {
    auto& memory = processor.getMemory();
    uint8_t address = getAddress(command);
    auto[result, carry, digitCarry] = arithmetic<ArithType::SUB>(CA_PASS, memory.read(address), memory.readWRegister());
    storeAtDestination(CA_PASS, address, result);
    updateFlags(CA_PASS, carry, digitCarry);
}

void Commands::swapf(CARGS) {
    uint8_t address = getAddress(command);
    uint8_t result = processor.getMemory().read(address);
    result = result << NIBBLE_BITS | result >> NIBBLE_BITS;
    storeAtDestination<false>(CA_PASS, address, result);
}

void Commands::xorwf(CARGS) {
    auto& memory = processor.getMemory();
    uint8_t address = getAddress(command);
    uint8_t result = memory.readWRegister() ^ memory.read(address);
    storeAtDestination(CA_PASS, address, result);
}

void Commands::bcf(CARGS) {
    changeBit<BitState::CLR>(CA_PASS);
}

void Commands::bsf(CARGS) {
    changeBit<BitState::SET>(CA_PASS);
}

void Commands::btfsc(CARGS) {
    testBit<BitState::CLR>(CA_PASS);
}

void Commands::btfss(CARGS) {
    testBit<BitState::SET>(CA_PASS);
}

void Commands::addlw(CARGS) {
    auto& memory = processor.getMemory();
    auto[result, carry, digitCarry] = arithmetic<ArithType::ADD>(CA_PASS, getLiteral(command), memory.readWRegister());
    memory.writeWRegister<true>(result);
    updateFlags(CA_PASS, carry, digitCarry);
}

void Commands::andlw(CARGS) {
    auto& memory = processor.getMemory();
    memory.writeWRegister<true>(memory.readWRegister() & getLiteral(command));
}

void Commands::call(CARGS) {
    processor.getStack().pushToStack(processor.getCommandMemory().getProgramCounter() + 1);
    jumpToLiteral(CA_PASS);
}

void Commands::clrwdt(CARGS) {
    clearWatchdogTimer(CA_PASS);
    setBit<Memory::STATUS_BITS::POWER_DOWN, true>(processor.getMemory().direct(Memory::STATUS));
}

void Commands::gotoo(CARGS) {
    jumpToLiteral(CA_PASS);
}

void Commands::iorlw(CARGS) {
    auto& memory = processor.getMemory();
    memory.writeWRegister<true>(memory.readWRegister() | getLiteral(command));
}

void Commands::movlw(CARGS) {
    processor.getMemory().writeWRegister(getLiteral(command));
}

void Commands::retfie(CARGS) {
    setBit<Memory::INTCON_BITS::GIE, true>(processor.getMemory().direct(Memory::INTCON));
    reeturn(CA_PASS);
}

void Commands::retlw(CARGS) {
    movlw(CA_PASS);
    reeturn(CA_PASS);
}

void Commands::reeturn(CARGS) {
    uint16_t target = processor.getStack().popFromStack();
    jumpToAddress(CA_PASS, target);
}

void Commands::sleep(CARGS) {
    clearWatchdogTimer(CA_PASS);
    setBit<Memory::STATUS_BITS::POWER_DOWN, false>(processor.getMemory().direct(Memory::STATUS));
    processor.getControlUnit().putToSleep();
}

void Commands::sublw(CARGS) {
    auto& memory = processor.getMemory();
    auto[result, carry, digitCarry] = arithmetic<ArithType::SUB>(CA_PASS, getLiteral(command), memory.readWRegister());
    memory.writeWRegister<true>(result);
    updateFlags(CA_PASS, carry, digitCarry);
}

void Commands::xorlw(CARGS) {
    auto& memory = processor.getMemory();
    memory.writeWRegister<true>(memory.readWRegister() ^ getLiteral(command));
}

template<bool UPDATE_ZERO_FLAG>
void Commands::storeAtDestination(CARGS, uint8_t address, uint8_t value) {
    auto& memory = processor.getMemory();
    if (getDestination(command)) {
        memory.write<UPDATE_ZERO_FLAG>(address, value);
    } else {
        memory.writeWRegister<UPDATE_ZERO_FLAG>(value);
    }
}

void Commands::updateFlags(CARGS, bool carry, bool digitCarry) {
    auto& memory = processor.getMemory();
    memory.updateCarryFlag(carry);
    memory.updateDigitCarryFlag(digitCarry);
}

template<Commands::CrementType C, bool UPDATE_ZERO_FLAG>
uint8_t Commands::crement(CARGS) {
    using OP = typename std::conditional<C == CrementType::INC, std::plus<uint8_t>, std::minus<uint8_t>>::type;
    OP op;
    uint8_t address = getAddress(command);
    uint8_t result = op(processor.getMemory().read(address), 1);
    storeAtDestination<UPDATE_ZERO_FLAG>(CA_PASS, address, result);
    return result;
}

template<Commands::RotationType R>
void Commands::rotate(CARGS) {
    auto& memory = processor.getMemory();
    uint8_t address = getAddress(command);
    uint8_t result = memory.read(address);
    uint8_t& status = memory.direct(Memory::STATUS);
    bool carry = getBit<Memory::CARRY>(memory.read(Memory::STATUS));
    if constexpr (R == RotationType::RIGHT) {
        setBitTo(status, Memory::CARRY, getBit<0>(result));
        result >>= 1;
        setBitTo(result, Memory::BYTE_BITS - 1, carry);
    } else {
        setBitTo(status, Memory::CARRY, getBit<Memory::BYTE_BITS - 1>(result));
        result <<= 1;
        setBitTo(result, 0, carry);
    }
    storeAtDestination<false>(CA_PASS, address, result);
}

template<Commands::BitState STATE>
void Commands::changeBit(CARGS) {
    auto& memory = processor.getMemory();
    uint8_t address = getAddress(command);
    uint8_t result = memory.read(address);
    uint8_t bit = getBitIndex(command);
    setBit<STATE == BitState::SET>(result, bit);
    memory.write(address, result);
}

template<Commands::BitState CONDITION>
void Commands::testBit(CARGS) {
    bool complies = getBit(processor.getMemory().read(getAddress(command)), getBitIndex(command));
    if constexpr (CONDITION == BitState::CLR) {
        complies = !complies;
    }
    if (complies) {
        skip(CA_PASS);
    }
}

void Commands::jumpToLiteral(CARGS) {
    uint16_t label = getLabel(command);
    uint16_t pcLath = processor.getMemory().direct(Memory::PCLATH);
    pcLath = getAndMasked(pcLath >> PCLATH_LSB, PCLATH_SIZE) << CMD_LABEL_BITS;
    uint16_t target = pcLath | label;
    jumpToAddress(CA_PASS, target);
}

void Commands::jumpToAddress(CARGS, uint16_t target) {
    processor.getCommandMemory().setProgramCounter(target - 2);
    skip(CA_PASS);
}

void Commands::skip(CARGS) {
    processor.getControlUnit().setNextCommand(NOP);
}

void Commands::clearWatchdogTimer(CARGS) {
    processor.getWatchdogTimer().clear();
    setBit<Memory::STATUS_BITS::TIME_OUT, true>(processor.getMemory().direct(Memory::STATUS));
}

template<Commands::ArithType A>
std::tuple<uint8_t, bool, bool> Commands::arithmetic(CARGS, uint8_t op1, uint8_t op2) {
    constexpr bool IS_SUB = A == ArithType::SUB;
    using OP = typename std::conditional<IS_SUB, std::minus<uint8_t>, std::plus<uint8_t>>::type;
    using LOP = typename std::conditional<IS_SUB, std::minus<uint16_t>, std::plus<uint16_t>>::type;
    // inversed flags for minus
    using EQ = typename std::conditional<IS_SUB, std::not_equal_to<uint16_t>, std::equal_to<uint16_t>>::type;

    OP op;
    LOP lop;
    EQ eq;

    auto& memory = processor.getMemory();
    uint8_t result = op(op1, op2);
    uint16_t longResult = lop(op1, op2);
    bool carry = !eq(result, longResult);
    uint8_t shortResult = op(getAndMasked(op1, NIBBLE_BITS), getAndMasked(op2, NIBBLE_BITS));
    bool digitCarry = eq(true, getBit<NIBBLE_BITS>(shortResult));
    return { result, carry, digitCarry };
}

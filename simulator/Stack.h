//
// Created by ced on 26.04.22.
//

#ifndef PICSIMULATOR_STACK_H
#define PICSIMULATOR_STACK_H


#include <cstdint>
#include <array>
#include <string>
#include <stdexcept>


class Processor;

class Stack {
public:
    constexpr static uint8_t STACK_BITS = 3;
    constexpr static uint8_t STACK_SIZE = 1 << STACK_BITS;

    explicit Stack(Processor& processor);
    void powerReset();
    void pushToStack(uint16_t value);
    uint16_t popFromStack();
    uint16_t readStack(uint8_t address);
    void writeStack(uint8_t address, uint16_t value);
    uint8_t getStackPointer();
private:
    Processor& processor_;
    uint8_t stackPointer_ = 0;
    std::array<uint16_t, STACK_SIZE> stack_{};

    uint16_t& accessStack(uint8_t address);
};


#include "Processor.h"


#endif //PICSIMULATOR_STACK_H

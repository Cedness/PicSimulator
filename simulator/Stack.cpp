//
// Created by ced on 26.04.22.
//

#include "Stack.h"
#include "../utility/utility.h"
#include <fstream>
#include <sstream>
#include <iostream>


Stack::Stack(Processor& processor)
        : processor_(processor) {}

void Stack::powerReset() {
    stack_.fill(0);
    stackPointer_ = 0;
}

void Stack::pushToStack(uint16_t value) {
    accessStack(stackPointer_++) = value;
}

uint16_t Stack::popFromStack() {
    return accessStack(--stackPointer_);
}

uint16_t Stack::readStack(uint8_t address) {
    return accessStack(address);
}

void Stack::writeStack(uint8_t address, uint16_t value) {
    accessStack(address) = value;
}

uint16_t& Stack::accessStack(uint8_t address) {
    return stack_.at(address % STACK_SIZE);
}

uint8_t Stack::getStackPointer() {
    return stackPointer_ % STACK_SIZE;
}

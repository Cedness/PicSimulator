//
// Created by ced on 12.04.22.
//

#ifndef PICSIMULATOR_COMMANDMEMORY_H
#define PICSIMULATOR_COMMANDMEMORY_H


#include "../utility/utility.h"
#include <cstdint>
#include <array>
#include <vector>
#include <string>
#include <string_view>
#include <stdexcept>


class Processor;

class CommandMemory {
    struct Line {
        std::string line;
        uint16_t address = -1;
    };
    struct Entry {
        uint16_t command = 0;
        size_t lineNumber = 0;
        bool breakpoint = false;
        bool written = false;
    };
public:
    constexpr static uint8_t ADDRESS_BITS = 13;
    constexpr static uint16_t ADDRESS_SIZE = 1 << ADDRESS_BITS;
    constexpr static uint8_t COMMAND_BITS = 14;
    constexpr static uint16_t COMMAND_SIZE = 1 << COMMAND_BITS;
    constexpr static uint8_t MEMORY_BITS = 10;
    constexpr static uint16_t MEMORY_SIZE = 1 << MEMORY_BITS;
    constexpr static uint16_t START_ADDRESS = 0;
    constexpr static uint16_t ISR_ADDRESS = 0x0004;
    constexpr static uint16_t CONFIG_ADDRESS = 0x2007;
    enum CONFIG_BITS : uint8_t {
        FOSC0 = 0, FOSC1, WDTE, PWRTE
    };

    explicit CommandMemory(Processor& processor);
    void powerReset();
    void softReset();
    void writeFileToMemory(const std::string& path, bool limitLineCount = false);
    void clearProgram();
    uint16_t getProgramCounter();
    void setProgramCounter(uint16_t value);
    uint16_t getValidProgramCounter();
    uint16_t read(uint16_t address);
    bool isBreakpoint(uint16_t address);
    void setBreakpoint(uint16_t address, bool breakpoint);
    uint16_t& getConfig();
    uint16_t getHighestWrittenAddress();
    size_t getLineCount();
    std::string& getLine(size_t lineNumber);
    uint16_t lineNumberToAddress(size_t lineNumber);
    size_t addressToLineNumber(uint16_t address);
    void printCommand(std::ostream& stream, uint16_t address);
    std::string strCommand(uint16_t address);
    void print(std::ostream& stream);
    std::string str();
private:
    Processor& processor_;
    uint16_t programCounter_ = 0;
    std::vector<Line> program_{ 1 };
    std::array<Entry, MEMORY_SIZE> memory_{};
    uint16_t highestWrittenAddress_ = 0;
    uint16_t config_ = getMask<COMMAND_BITS, uint16_t>();

    void reset();
    inline Entry& accessMemory(uint16_t address);
    inline uint16_t& accessCommand(uint16_t address);
};


#include "Processor.h"


#endif //PICSIMULATOR_COMMANDMEMORY_H

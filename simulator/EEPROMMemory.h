//
// Created by ced on 10.05.22.
//

#ifndef PICSIMULATOR_EEPROMMEMORY_H
#define PICSIMULATOR_EEPROMMEMORY_H


#include "../utility/utility.h"
#include <cstdint>
#include <array>
#include <chrono>


class Processor;

class EEPROMMemory {
public:
    constexpr static uint8_t ADDRESS_BITS = 8;
    constexpr static uint16_t ADDRESS_SIZE = getSetBit<0, ADDRESS_BITS, true>();
    constexpr static uint8_t MEMORY_BITS = 6;
    constexpr static uint16_t MEMORY_SIZE = getSetBit<0, MEMORY_BITS, true>();
    constexpr static std::chrono::nanoseconds READ_DELAY{ 0 };
    constexpr static std::chrono::nanoseconds WRITE_DELAY = std::chrono::milliseconds(1);
    constexpr static uint8_t SECURITY_STAGE_COUNT = 2;
    constexpr static std::array<uint8_t, SECURITY_STAGE_COUNT> SECURITY_NUMBERS{ 0x55, 0xaa };
    explicit EEPROMMemory(Processor& processor);
    void powerReset();
    void doCycle();
    uint8_t read(uint8_t address);
    void write(uint8_t address, uint8_t value);
    void eecon1Write(uint8_t value);
    void eecon2Write(uint8_t value);
private:
    Processor& processor_;
    std::array<uint8_t, MEMORY_SIZE> memory_{};
    std::chrono::nanoseconds doneAt_{ 0 };
    uint8_t securityStage = 0;

    bool securityUnlocked();
    void processRequest(uint8_t& value);
    void doRead();
    void doWrite();
};


#include "Processor.h"


#endif //PICSIMULATOR_EEPROMMEMORY_H

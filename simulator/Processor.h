//
// Created by ced on 04.04.22.
//

#ifndef PICSIMULATOR_PROCESSOR_H
#define PICSIMULATOR_PROCESSOR_H


#include "Memory.h"
#include "Stack.h"
#include "CommandMemory.h"
#include "ControlUnit.h"
#include "Prescaler.h"
#include "Timer0.h"
#include "WatchdogTimer.h"
#include "IOPorts.h"
#include "EEPROMMemory.h"
#include <cstdint>
#include <vector>
#include <array>
#include <memory>


class Commands;

class Processor {
    std::shared_ptr<Commands> commands_;
    Memory memory_ = Memory(*this);
    Stack stack_ = Stack(*this);
    CommandMemory commandMemory_ = CommandMemory(*this);
    ControlUnit controlUnit_ = ControlUnit(*this);
    Prescaler prescaler_ = Prescaler(*this);
    Timer0 timer0_ = Timer0(*this);
    WatchdogTimer watchdogTimer_ = WatchdogTimer(*this);
    IOPorts ioPorts_ = IOPorts(*this);
    EEPROMMemory eepromMemory_ = EEPROMMemory(*this);
public:
    Processor(std::shared_ptr<Commands> commands);
    Commands& getCommands();
    Memory& getMemory();
    const Memory& getMemory() const;
    Stack& getStack();
    CommandMemory& getCommandMemory();
    ControlUnit& getControlUnit();
    Prescaler& getPrescaler();
    Timer0& getTimer0();
    WatchdogTimer& getWatchdogTimer();
    IOPorts& getIOPorts();
    const IOPorts& getIOPorts() const;
    EEPROMMemory& getEEPROMMemory();
};


#define PICSIMULATOR_PROCESSOR_H_DEF


#include "Memory.h"
#include "Stack.h"
#include "CommandMemory.h"
#include "ControlUnit.h"
#include "Prescaler.h"
#include "Timer0.h"
#include "WatchdogTimer.h"
#include "IOPorts.h"
#include "EEPROMMemory.h"


#undef PICSIMULATOR_PROCESSOR_H_DEF


#endif //PICSIMULATOR_PROCESSOR_H
// copy

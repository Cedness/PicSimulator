//
// Created by ced on 02.05.22.
//

#ifndef PICSIMULATOR_WATCHDOGTIMER_H
#define PICSIMULATOR_WATCHDOGTIMER_H


#include <cstdint>
#include <chrono>


class Processor;

class WatchdogTimer {
public:
    constexpr static uint8_t PRESCALER_BASE = 1;
    explicit WatchdogTimer(Processor& processor);
    void powerReset();
    void doCycle();
    void clear();
    int64_t get();
private:
    constexpr static std::chrono::nanoseconds TIMEOUT_PERIOD = std::chrono::milliseconds(18);
    Processor& processor_;
    std::chrono::nanoseconds timeout_ = TIMEOUT_PERIOD;
};


#include "Processor.h"


#endif //PICSIMULATOR_WATCHDOGTIMER_H

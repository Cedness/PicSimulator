//
// Created by ced on 05.04.22.
//

#ifndef PICSIMULATOR_COMMANDS_H
#define PICSIMULATOR_COMMANDS_H


#include "../utility/utility.h"
#include "Processor.h"
#include <cstdint>
#include <string>
#include <iostream>
#include <array>
#include <functional>
#include <memory>
#include <tuple>


typedef uint16_t Command;
#define CA_PASS command, commandData, processor
#define CARGS Command command, const Commands::CommandData& commandData, Processor& processor
#define CARGS_FULL Commands* commands, CARGS

class Commands {
public:
    struct CommandData {
        constexpr static char ZERO = '0';
        constexpr static char ONE = '1';
        constexpr static char REDUNDANT = 'x';
        constexpr static char DESTINATION = 'd';
        constexpr static char FILE = 'f';
        constexpr static char LITERAL = 'k';
        constexpr static char BIT = 'b';
        static const std::string ALLOWED_CHARS;
        std::string name;
        uint16_t code;
        std::string binary;
        std::string hex;
        bool hasLiteral = false;
        bool hasAddress = false;
        bool hasLabel = false;
        bool hasDestination = false;
        bool hasBits = false;
        std::function<void(CARGS_FULL)> microcode;
        CommandData(Commands* commands, std::string&& name, std::string&& binary, std::function<void(CARGS_FULL)> microcode);
        inline friend std::stringstream& operator<<(std::stringstream &out, const CommandData& c);
        inline friend std::ostream& operator<<(std::ostream &out, const CommandData& c);
        std::string str();
        operator Command() const;
    private:
        template<uint8_t BIT, char SYMBOL>
        inline bool check();
    };

    constexpr static uint8_t COMMAND_MEMORY_BITS = 10;
    constexpr static uint16_t COMMAND_MEMORY_SIZE = 1 << COMMAND_MEMORY_BITS;
    //
    constexpr static uint8_t COMMAND_BITS = 14;
    constexpr static uint8_t INDEXED_BITS = 6;
    constexpr static uint8_t VARIABLE_BITS = COMMAND_BITS - INDEXED_BITS;
    constexpr static uint16_t COMMAND_SIZE = 1 << COMMAND_BITS;
    constexpr static uint16_t INDEX_SIZE = 1 << INDEXED_BITS;
    constexpr static uint16_t VARIABLE_SIZE = 1 << VARIABLE_BITS;
    //
    constexpr static char CMD_ADDRESS_BITS = 7;
    constexpr static char CMD_DESTINATION_BIT = CMD_ADDRESS_BITS;
    constexpr static char CMD_LITERAL_BITS = 8;
    constexpr static char CMD_LABEL_BITS = 11;
    constexpr static char CMD_BIT_BITS = 3;
    constexpr static char CMD_BIT_FROM = CMD_ADDRESS_BITS;
    constexpr static char CMD_BIT_TO = CMD_BIT_FROM + CMD_BIT_BITS;
    //
    constexpr static uint8_t PCLATH_LSB = 2;
    constexpr static uint8_t PCLATH_SIZE = 2;
    //
    constexpr static uint8_t NIBBLE_BITS = 4;
private:
    std::array<std::vector<const CommandData*>, INDEX_SIZE> commandsData_{};
public:
    using CMD_DATA = const CommandData;
    CMD_DATA ADDWF = CommandData(this, "addwf", "000111dfffffff", &Commands::addwf);
    CMD_DATA ANDWF = CommandData(this, "andwf", "000101dfffffff", &Commands::andwf);
    CMD_DATA CLRF = CommandData(this, "clrf", "0000011fffffff", &Commands::clrf);
    CMD_DATA CLRW = CommandData(this, "clrw", "0000010xxxxxxx", &Commands::clrw);
    CMD_DATA COMF = CommandData(this, "comf", "001001dfffffff", &Commands::comf);
    CMD_DATA DECF = CommandData(this, "decf", "000011dfffffff", &Commands::decf);
    CMD_DATA DECFSZ = CommandData(this, "decfsz", "001011dfffffff", &Commands::decfsz);
    CMD_DATA INCF = CommandData(this, "incf", "001010dfffffff", &Commands::incf);
    CMD_DATA INCFSZ = CommandData(this, "incfsz", "001111dfffffff", &Commands::incfsz);
    CMD_DATA IORWF = CommandData(this, "iorwf", "000100dfffffff", &Commands::iorwf);
    CMD_DATA MOVF = CommandData(this, "movf", "001000dfffffff", &Commands::movf);
    CMD_DATA MOVWF = CommandData(this, "movwf", "0000001fffffff", &Commands::movwf);
    CMD_DATA NOP = CommandData(this, "nop", "0000000xx00000", &Commands::nop);
    CMD_DATA RLF = CommandData(this, "rlf", "001101dfffffff", &Commands::rlf);
    CMD_DATA RRF = CommandData(this, "rrf", "001100dfffffff", &Commands::rrf);
    CMD_DATA SUBWF = CommandData(this, "subwf", "000010dfffffff", &Commands::subwf);
    CMD_DATA SWAPF = CommandData(this, "swapf", "001110dfffffff", &Commands::swapf);
    CMD_DATA XORWF = CommandData(this, "xorwf", "000110dfffffff", &Commands::xorwf);
    CMD_DATA BCF = CommandData(this, "bcf", "0100bbbfffffff", &Commands::bcf);
    CMD_DATA BSF = CommandData(this, "bsf", "0101bbbfffffff", &Commands::bsf);
    CMD_DATA BTFSC = CommandData(this, "btfsc", "0110bbbfffffff", &Commands::btfsc);
    CMD_DATA BTFSS = CommandData(this, "btfss", "0111bbbfffffff", &Commands::btfss);
    CMD_DATA ADDLW = CommandData(this, "addlw", "11111xkkkkkkkk", &Commands::addlw);
    CMD_DATA ANDLW = CommandData(this, "andlw", "111001kkkkkkkk", &Commands::andlw);
    CMD_DATA CALL = CommandData(this, "call", "100kkkkkkkkkkk", &Commands::call);
    CMD_DATA CLRWDT = CommandData(this, "clrwdt", "00000001100100", &Commands::clrwdt);
    CMD_DATA GOTOO = CommandData(this, "goto", "101kkkkkkkkkkk", &Commands::gotoo);
    CMD_DATA IORLW = CommandData(this, "iorlw", "111000kkkkkkkk", &Commands::iorlw);
    CMD_DATA MOVLW = CommandData(this, "movlw", "1100xxkkkkkkkk", &Commands::movlw);
    CMD_DATA RETFIE = CommandData(this, "retfie", "00000000001001", &Commands::retfie);
    CMD_DATA RETLW = CommandData(this, "retlw", "1101xxkkkkkkkk", &Commands::retlw);
    CMD_DATA REETURN = CommandData(this, "return", "00000000001000", &Commands::reeturn);
    CMD_DATA SLEEP = CommandData(this, "sleep", "00000001100011", &Commands::sleep);
    CMD_DATA SUBLW = CommandData(this, "sublw", "11110xkkkkkkkk", &Commands::sublw);
    CMD_DATA XORLW = CommandData(this, "xorlw", "111010kkkkkkkk", &Commands::xorlw);

    static std::shared_ptr<Commands> get();
    Commands();
    Commands(const Commands&) = delete;
    Commands& operator=(const Commands&) = delete;
    const CommandData& getCommandData(Command command);
    uint8_t getLiteral(Command command);
    uint8_t getAddress(Command command);
    uint16_t getLabel(Command command);
    uint8_t getBitIndex(Command command);
    bool getDestination(Command command);
    std::string toString(Command command);
    std::string toString(Command command, const CommandData& commandData);
private:
    enum class ArithType {
        SUB, ADD
    };
    enum class RotationType {
        LEFT, RIGHT
    };
    enum class CrementType {
        DEC, INC
    };
    enum class BitState {
        SET, CLR
    };
    static std::weak_ptr<Commands> INSTANCE;

    void addwf(CARGS);
    void andwf(CARGS);
    void clrf(CARGS);
    void clrw(CARGS);
    void comf(CARGS);
    void decf(CARGS);
    void decfsz(CARGS);
    void incf(CARGS);
    void incfsz(CARGS);
    void iorwf(CARGS);
    void movf(CARGS);
    void movwf(CARGS);
    void nop(CARGS);
    void rlf(CARGS);
    void rrf(CARGS);
    void subwf(CARGS);
    void swapf(CARGS);
    void xorwf(CARGS);
    void bcf(CARGS);
    void bsf(CARGS);
    void btfsc(CARGS);
    void btfss(CARGS);
    void addlw(CARGS);
    void andlw(CARGS);
    void call(CARGS);
    void clrwdt(CARGS);
    void gotoo(CARGS);
    void iorlw(CARGS);
    void movlw(CARGS);
    void retfie(CARGS);
    void retlw(CARGS);
    void reeturn(CARGS);
    void sleep(CARGS);
    void sublw(CARGS);
    void xorlw(CARGS);
    template<bool UPDATE_ZERO_FLAG = true>
    inline void storeAtDestination(CARGS, uint8_t address, uint8_t value);
    inline void updateFlags(CARGS, bool carry, bool digitCarry);
    template<CrementType C, bool UPDATE_ZERO_FLAG = true>
    inline uint8_t crement(CARGS);
    template<RotationType R>
    inline void rotate(CARGS);
    template<BitState STATE>
    inline void changeBit(CARGS);
    template<BitState CONDITION>
    inline void testBit(CARGS);
    inline void jumpToLiteral(CARGS);
    inline void jumpToAddress(CARGS, uint16_t target);
    inline void skip(CARGS);
    inline void clearWatchdogTimer(CARGS);
    template<ArithType A>
    inline std::tuple<uint8_t, bool, bool> arithmetic(CARGS, uint8_t op1, uint8_t op2);
};


#endif //PICSIMULATOR_COMMANDS_H

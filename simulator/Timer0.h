//
// Created by Ced on 01/05/2022.
//

#ifndef PICSIMULATOR_TIMER0_H
#define PICSIMULATOR_TIMER0_H


#include <cstdint>


class Processor;

class Timer0 {
public:
    constexpr static uint8_t PRESCALER_BASE = 2;
    explicit Timer0(Processor& processor);
    void powerReset();
    void doCycle();
    void onT0CKI(bool risingEdge);
    void setWritten();
private:
    constexpr static uint8_t INHIBIT_CYCLES_AFTER_WRITE = 2;

    Processor& processor_;
    uint8_t inhibitCycles_ = 0;

    void count(uint8_t option);
};


#include "Processor.h"


#endif //PICSIMULATOR_TIMER0_H

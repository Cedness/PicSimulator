//
// Created by ced on 04.04.22.
//

#include "Processor.h"
#include <utility>


Processor::Processor(std::shared_ptr<Commands> commands)
        : commands_(std::move(commands)) {}

Commands& Processor::getCommands() {
    return *commands_;
}

Memory& Processor::getMemory() {
    return memory_;
}

const Memory& Processor::getMemory() const {
    return memory_;
}

Stack& Processor::getStack() {
    return stack_;
}

CommandMemory& Processor::getCommandMemory() {
    return commandMemory_;
}

ControlUnit& Processor::getControlUnit() {
    return controlUnit_;
}

Prescaler& Processor::getPrescaler() {
    return prescaler_;
}

Timer0& Processor::getTimer0() {
    return timer0_;
}

WatchdogTimer& Processor::getWatchdogTimer() {
    return watchdogTimer_;
}

IOPorts& Processor::getIOPorts() {
    return ioPorts_;
}

const IOPorts& Processor::getIOPorts() const {
    return ioPorts_;
}

EEPROMMemory& Processor::getEEPROMMemory() {
    return eepromMemory_;
}

//
// Created by Ced on 01/05/2022.
//

#include "IOPorts.h"

IOPorts::IOPorts(Processor& processor)
        : processor_(processor), portA_(*this, Memory::PORTA, Memory::TRISA), portB_(*this, Memory::PORTB, Memory::TRISB) {}

void IOPorts::powerReset() {
    port<A>().powerReset();
    port<B>().powerReset();
}

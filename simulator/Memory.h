//
// Created by ced on 05.04.22.
//

#ifndef PICSIMULATOR_PROCESSOR_H_DEF
#ifndef PICSIMULATOR_MEMORY_H_DEC
#define PICSIMULATOR_MEMORY_H_DEC


#include <cstdint>
#include <cstdlib>
#include <array>


class Processor;

class Memory {
public:
    constexpr static uint8_t BYTE_BITS = 8;
    constexpr static uint8_t INDIRECT_ADDR = 0x00;
    constexpr static uint8_t TMR0 = 0x01;
    constexpr static uint8_t OPTION = 0x81;
    enum OPTION_BITS : uint8_t {
        PS0 = 0, PS1, PS2, PSA, T0SE, T0CS, INTEDG, RBPU
    };
    constexpr static uint8_t PCL = 0x02;
    constexpr static uint8_t STATUS = 0x03;
    enum STATUS_BITS : uint8_t {
        CARRY = 0, DIGIT_CARRY, ZERO, POWER_DOWN, TIME_OUT, RP0, RP1, IRP
    };
    constexpr static uint8_t FSR = 0x04;
    constexpr static uint8_t PORTA = 0x05;
    enum PORTA_BITS : uint8_t {
        RA0 = 0, RA1, RA2, RA3, RA4_T0CKI
    };
    constexpr static uint8_t TRISA = 0x85;
    constexpr static uint8_t PORTB = 0x06;
    enum PORTB_BITS : uint8_t {
        RB0_INT = 0, RB1, RB2, RB3, RB4, RB5, RB6, RB7
    };
    constexpr static uint8_t TRISB = 0x86;
    constexpr static uint8_t AN_UNIMPLEMENTED_LOCATION = 0x07;
    constexpr static uint8_t EEDATA = 0x08;
    constexpr static uint8_t EECON1 = 0x88;
    enum EECON1_BITS : uint8_t {
        RD = 0, WR, WREN, WRERR, EEIF
    };
    constexpr static uint8_t EEADR = 0x09;
    constexpr static uint8_t EECON2 = 0x89;
    constexpr static uint8_t PCLATH = 0x0a;
    constexpr static uint8_t INTCON = 0x0b;
    enum INTCON_BITS : uint8_t {
        RBIF = 0, INTF, T0IF, RBIE, INTE, T0IE, EEIE, GIE
    };

    explicit Memory(Processor& processor);
    void powerReset();
    void softReset();
    uint8_t readWRegister();
    template<bool UPDATE_ZERO_FLAG = false>
    void writeWRegister(uint8_t value);
    uint8_t& direct(uint8_t address);
    uint8_t direct(uint8_t address) const;
    uint8_t get(uint8_t address) const;
    void set(uint8_t address, uint8_t value);
    uint8_t read(uint8_t address);
    template<bool UPDATE_ZERO_FLAG = false>
    void write(uint8_t address, uint8_t value);
    void updateZeroFlag(uint8_t value);
    void updateCarryFlag(bool carry);
    void updateDigitCarryFlag(bool digitCarry);
private:
    constexpr static size_t BANK_SIZE = 0x80;
    constexpr static size_t MEMORY_SIZE = 2 * BANK_SIZE;
    constexpr static size_t USABLE_BANK_SIZE = 0x50;
    constexpr static size_t USABLE_MEMORY_SIZE = 2 * USABLE_BANK_SIZE;
    constexpr static size_t BANK_ADDRESS_BIT = 7;
    constexpr static size_t PARTLY_IMPLEMENTED_BITS = 5;

    Processor& processor_;
    uint8_t wRegister_ = 0;
    std::array<uint8_t*, MEMORY_SIZE> memoryPointers_;
    std::array<uint8_t, USABLE_MEMORY_SIZE> memory_{};
    uint8_t unimplementedMemory_ = 0;

    template<uint8_t ADDRESS>
    void setEqual();
    void initialize();
    uint8_t getBank();
    void calcBankAddress(uint8_t& address);
    void calcIndirectAddress(uint8_t& address) const;
    template<bool UPDATE_ZERO_FLAG>
    void writeInternal(uint8_t address, uint8_t& value);
};


#include "Processor.h"


#endif //PICSIMULATOR_MEMORY_H_DEC
#endif //PICSIMULATOR_PROCESSOR_H_DEF

#ifdef PICSIMULATOR_PROCESSOR_H_DEF
#ifndef PICSIMULATOR_MEMORY_H_DEF
#define PICSIMULATOR_MEMORY_H_DEF


#include "../utility/utility.h"


// special case to check for an address in all banks
#define CAISE(x) case x: case getSetBit<true>(x, BANK_ADDRESS_BIT)

template<bool UPDATE_ZERO_FLAG>
void Memory::writeWRegister(uint8_t value) {
    wRegister_ = value;
    if constexpr (UPDATE_ZERO_FLAG)
        updateZeroFlag(value);
}

template<bool UPDATE_ZERO_FLAG>
void Memory::write(uint8_t address, uint8_t value) {
    calcBankAddress(address);
    calcIndirectAddress(address);
    writeInternal<UPDATE_ZERO_FLAG>(address, value);
    if constexpr (UPDATE_ZERO_FLAG) {
        // do this after writing so it is not overwritten when writing to STATUS
        updateZeroFlag(value);
    }
}

template<uint8_t ADDRESS>
void Memory::setEqual() {
    this->memoryPointers_.at(getSetBit<true>(ADDRESS, BANK_ADDRESS_BIT)) = this->memoryPointers_.at(ADDRESS);
}

template<bool UPDATE_ZERO_FLAG>
void Memory::writeInternal(uint8_t address, uint8_t& value) {
    switch (address) {
        CAISE(INDIRECT_ADDR):
            // don't allow indirect write when FSR points to 0
            return;
        case (TMR0):
            processor_.getTimer0().setWritten();
            break;
        CAISE(PCL): {
            uint16_t programCounter = direct(PCLATH);
            programCounter <<= BYTE_BITS;
            programCounter |= value;
            auto& commandMemory = processor_.getCommandMemory();
            commandMemory.setProgramCounter(programCounter);
            processor_.getControlUnit().setNextCommand(commandMemory.read(commandMemory.getProgramCounter() + 1));
            return;
        }
        CAISE(STATUS): {
            uint8_t mask;
            if constexpr (UPDATE_ZERO_FLAG) {
                // don't allow write to zero, carry and digitcarry for commands that update these flags
                mask = getMask<STATUS_BITS::RP0, uint8_t>();
            } else {
                // don't allow write to softReset status bits
                mask = getMask<STATUS_BITS::POWER_DOWN, STATUS_BITS::RP0, uint8_t>();
            }
            value = mask & direct(STATUS) | ~mask & value;
            break;
        }
        case PORTA:
            andMask(value, PARTLY_IMPLEMENTED_BITS);
            processor_.getIOPorts().port<IOPorts::A>().write(value);
            return;
        case TRISA:
            andMask(value, PARTLY_IMPLEMENTED_BITS);
            processor_.getIOPorts().port<IOPorts::A>().writeTris(value);
            break;
        case PORTB:
            processor_.getIOPorts().port<IOPorts::B>().write(value);
            return;
        case TRISB:
            processor_.getIOPorts().port<IOPorts::B>().writeTris(value);
            break;
        case EECON1:
            andMask(value, PARTLY_IMPLEMENTED_BITS);
            processor_.getEEPROMMemory().eecon1Write(value);
            return;
        case EECON2:
            processor_.getEEPROMMemory().eecon2Write(value);
            return;
        CAISE(PCLATH):
            // change values written to unimplemented bits to zero
            andMask(value, PARTLY_IMPLEMENTED_BITS);
            break;
        CAISE(INTCON):
            processor_.getIOPorts().port<IOPorts::B>().checkPortBInterrupt(value);
        default:
            break;
    }
    uint8_t* memoryPointer = memoryPointers_.at(address);
    if (memoryPointer == &unimplementedMemory_) {
        // don't allow write to unimplemented memory
        return;
    }
    *memoryPointer = value;
}


#endif //PICSIMULATOR_MEMORY_H_DEF
#endif //PICSIMULATOR_PROCESSOR_H_DEF

//
// Created by ced on 12.04.22.
//

#include "CommandMemory.h"
#include "Commands.h"
#include "../utility/utility.h"
#include <fstream>
#include <iostream>


CommandMemory::CommandMemory(Processor& processor)
        : processor_(processor) {}

void CommandMemory::powerReset() {
    programCounter_ = START_ADDRESS;
}

void CommandMemory::softReset() {
    programCounter_ = START_ADDRESS;
}

void CommandMemory::writeFileToMemory(const std::string& path, bool limitLineCount) {
    std::ifstream file(path);
    if (!file.is_open())
        throw std::logic_error(F() << "Cannot open file: " << path);
    DEBUG_VERBOSE("Loading file " << path << "\n");

    // clear command memory
    reset();

    size_t lineLimit = limitLineCount ? std::numeric_limits<int>::max() : std::numeric_limits<size_t>::max();
    for (size_t lineCount = 0; file && lineCount < lineLimit; lineCount++) {
        unsigned long address;
        unsigned long command;
        Line& line = program_.emplace_back();
        std::getline(file, line.line);
        size_t end = line.line.find_last_not_of(" \r\n");
        line.line = line.line.substr(0, end + 1);

        if (line.line.empty() || line.line.rfind(' ', 0) == 0) {
            // Non-code line, skip
            continue;
        }
        std::istringstream iss(line.line);
        std::string word;
        if (iss >> word) {
            address = std::stoul(word, nullptr, 16);
        } else continue; // unable to convert address, skip
        if (iss >> word) {
            command = std::stoul(word, nullptr, 16);
        } else continue; // unable to convert command, skip
        if (address >= ADDRESS_SIZE || command >= COMMAND_SIZE) {
            // Out of range address / command, skip
            continue;
        }

        // Valid code line, save it
        line.address = address;
        if (address == CONFIG_ADDRESS) {
            config_ = command;
        } else {
            accessMemory(address) = {
                    static_cast<uint16_t>(command),
                    program_.size() - 1
            };
            if (address > highestWrittenAddress_) {
                highestWrittenAddress_ = address;
            }
        }
        DEBUG_VERBOSE(toHex16(address) << " " << toHex16(command) << "\n");
    }
    file.close();
    if (program_.empty()) {
        program_.emplace_back();
    }
    processor_.getControlUnit().powerOnReset();
    DEBUG_VERBOSE("Loading file done\n");
}

void CommandMemory::clearProgram() {
    reset();
    program_.emplace_back();
    processor_.getControlUnit().powerOnReset();
    DEBUG("Program cleared\n");
}

uint16_t CommandMemory::getProgramCounter() {
    return programCounter_;
}

void CommandMemory::setProgramCounter(uint16_t value) {
    programCounter_ = value % ADDRESS_SIZE;
    processor_.getMemory().direct(Memory::PCL) = programCounter_;
}

uint16_t CommandMemory::getValidProgramCounter() {
    return programCounter_ % MEMORY_SIZE;
}

uint16_t CommandMemory::read(uint16_t address) {
    return accessCommand(address);
}

bool CommandMemory::isBreakpoint(uint16_t address) {
    return accessMemory(address).breakpoint;
}

void CommandMemory::setBreakpoint(uint16_t address, bool breakpoint) {
    accessMemory(address).breakpoint = breakpoint;
}

uint16_t& CommandMemory::getConfig() {
    return config_;
}

uint16_t CommandMemory::getHighestWrittenAddress() {
    return highestWrittenAddress_;
}

size_t CommandMemory::getLineCount() {
    return program_.size();
}

std::string& CommandMemory::getLine(size_t lineNumber) {
    return program_.at(lineNumber).line;
}

uint16_t CommandMemory::lineNumberToAddress(size_t lineNumber) {
    return program_.at(lineNumber).address;
}

size_t CommandMemory::addressToLineNumber(uint16_t address) {
    return memory_.at(address).lineNumber;
}

void CommandMemory::printCommand(std::ostream& stream, uint16_t address) {
    Command command = accessCommand(address);
    stream << toHex16(address) << " " << toHex16(command);
    stream << "  " << processor_.getCommands().toString(command);
}

std::string CommandMemory::strCommand(uint16_t address) {
    std::stringstream sstream;
    printCommand(sstream, address);
    return sstream.str();
}

void CommandMemory::print(std::ostream& stream) {
    for (uint16_t address = 0; address < highestWrittenAddress_; address++) {
        auto& entry = memory_.at(address);
        if (!entry.written) {
            // Empty cell
            continue;
        }
        printCommand(stream, address);
        stream << "\n";
    }
}

std::string CommandMemory::str() {
    std::stringstream sstream;
    print(sstream);
    return sstream.str();
}

void CommandMemory::reset() {
    programCounter_ = 0;
    for (uint16_t i = 0; i < MEMORY_SIZE; i++) {
        accessMemory(i) = {};
    }
    config_ = getMask<COMMAND_BITS, uint16_t>();
    highestWrittenAddress_ = 0;
    program_.clear();
}

CommandMemory::Entry& CommandMemory::accessMemory(uint16_t address) {
    return memory_.at(address % MEMORY_SIZE);
}

uint16_t& CommandMemory::accessCommand(uint16_t address) {
    return accessMemory(address).command;
}

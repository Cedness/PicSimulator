//
// Created by ced on 12.04.22.
//

#ifndef PICSIMULATOR_CONTROLUNIT_H
#define PICSIMULATOR_CONTROLUNIT_H


#include <chrono>
#include <cstdint>


class Processor;

class ControlUnit {
public:
    constexpr static int64_t DEFAULT_FREQUENCY = 4000000;
    constexpr static int64_t CYCLES_PER_INSTRUCTION = 4;

    explicit ControlUnit(Processor& processor);
    void powerOnReset();
    void softReset();
    void doCycle();
    uint16_t getNextCommand();
    void setNextCommand(uint16_t nextCommand);
    bool isSleeping();
    bool isBreaking();
    void putToSleep();
    void wakeUp();
    int64_t getFrequency();
    void setFrequency(int64_t frequency);
    void setInstructionsPerSecond(int64_t instructionsPerSecond);
    std::chrono::nanoseconds getInstructionTime();
    uint64_t getCycle();
    std::chrono::nanoseconds getTime();
private:
    Processor& processor_;
    bool sleeping_ = false;
    bool breaking_ = false;
    uint64_t cycle_ = 0;
    std::chrono::nanoseconds time_{ 0 };
    int64_t instructionsPerSecond_;
    std::chrono::nanoseconds instructionTime_;
    uint16_t nextCommand_ = 0;

    void doInterrupts();
    void runCommand();
};


#include "Processor.h"


#endif //PICSIMULATOR_CONTROLUNIT_H

//
// Created by ced on 02.05.22.
//

#ifndef PICSIMULATOR_PRESCALER_H
#define PICSIMULATOR_PRESCALER_H


#include <cstdint>


class Processor;

class Prescaler {
public:
    explicit Prescaler(Processor& processor);
    void powerReset();
    uint8_t doCycle();
    uint8_t get();
    void clear();
private:
    Processor& processor_;
    uint8_t prescaler_ = 0;
};


#include "Processor.h"


#endif //PICSIMULATOR_PRESCALER_H

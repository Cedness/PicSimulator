//
// Created by ced on 02.05.22.
//

#include "Controller.h"
#include <cmath>
#include <algorithm>


/*
 * Constructor of Controller
 * loads stertup program if present
 * @param args command line arguments
 */
Controller::Controller(std::vector<std::string_view>& args) {
    if (args.size() >= 2) {
        programPath_ = args.at(1);
    }
    reload();
    mainLoop_.start();
}

/*
 * calls for one tick of the program clock
 */
void Controller::doCycle(MainLoopKey) {
    auto& controlUnit = processor_.getControlUnit();
    controlUnit.doCycle();
    if (controlUnit.isBreaking()) {
        mainLoop_.setRunning(false);
    }
}

/*
* gets data form memory and paces it on the gui
*/
void Controller::updateGUI(MainLoopKey) {
    mainWindow_.setLocked(true);
    auto& memory = processor_.getMemory();
    auto& controlUnit = processor_.getControlUnit();
    auto& commandMemory = processor_.getCommandMemory();
    auto& stack = processor_.getStack();
    for (uint16_t var = 0; var <= 255; var++) {
        mainWindow_.setMemory(var, memory.get(var));
    }
    mainWindow_.setW(memory.readWRegister());
    mainWindow_.setPCL(memory.get(Memory::PCL));
    mainWindow_.setPcLatch(memory.get(Memory::PCLATH));
    mainWindow_.setPC(commandMemory.getProgramCounter());
    mainWindow_.setSTPTR(stack.getStackPointer());
    mainWindow_.setStatus(memory.get(Memory::STATUS));
    mainWindow_.setFSR(memory.get(Memory::FSR));
    mainWindow_.setTimer0(memory.get(Memory::TMR0));
    mainWindow_.setWDT(processor_.getWatchdogTimer().get());
    mainWindow_.setPS(processor_.getPrescaler().get());
    for (uint8_t var = 0; var < 8; var++) {
        mainWindow_.setStack(var, stack.readStack(var));
    }
    mainWindow_.setPins(0, memory.get(Memory::PORTA));
    mainWindow_.setPins(1, memory.get(Memory::PORTB));
    mainWindow_.setPinToInput(0, memory.get(Memory::TRISA));
    mainWindow_.setPinToInput(1, memory.get(Memory::TRISB));
    mainWindow_.setClock(std::chrono::duration_cast<std::chrono::microseconds>(controlUnit.getTime()).count());
    mainWindow_.setFrequency(controlUnit.getFrequency());
    mainWindow_.setSimSpeedValue(mainLoop_.getCyclesPerSecond());
    mainWindow_.setSimSpeedSlider(simulationSpeedPeriodToLevel(mainLoop_.getCycleTime()));
    uint16_t nextCommand = controlUnit.getNextCommand();
    mainWindow_.setCurrentCommend(QString::fromStdString(F() << toHex16(nextCommand) << "h  |  " << processor_.getCommands().toString(nextCommand)));
    mainWindow_.colorCommand(static_cast<int>(commandMemory.addressToLineNumber(commandMemory.getValidProgramCounter())));
    mainWindow_.setWatchDog(getBit<CommandMemory::CONFIG_BITS::WDTE>(commandMemory.getConfig()));
    mainWindow_.setLocked(false);
}

/*
 * starts execution
 */
void Controller::start() {
    mainLoop_.setRunning(true);
}

/*
 * calls for the exicution of one command
 */
void Controller::step() {
    mainLoop_.step();
}

/*
 * stops execution
 */
void Controller::stop() {
    mainLoop_.setRunning(false);
}

/*
 * set program counter to zero and clear gpm
 */
void Controller::softReset() {
    processor_.getControlUnit().softReset();
    updateGUI({});
}

void Controller::reload() {
    auto& commandMemory = processor_.getCommandMemory();
    if (programPath_.empty()) {
        commandMemory.clearProgram();
    } else {
        commandMemory.writeFileToMemory(programPath_, true);
    }
    mainWindow_.loadCommands(getCommandStrings());
    colorCommandFirstCommand();
    updateGUI({});
}

/*
 * delivers path for test programs to memory
 * @param path to test program
 */
void Controller::setFilePath(const std::string& path) {
    if (path.empty()) {
        return;
    }
    programPath_ = path;
    reload();
}

void Controller::clearProgram() {
    programPath_ = {};
    reload();
}

/*
 * updates gpm memory if altered in gui
 * @param adresse of memory and value to set
 */
void Controller::updateMemory(uint8_t adress, int value) {
    if(value >= 0 && value < 256){
        auto& memory = processor_.getMemory();
        memory.set(adress, value);
    }
    updateGUI({});
}

/*
 * updates memory if for changed port values
 * @param values of port A and port B
 */
void Controller::updatePins(uint8_t pinsPortA, uint8_t pinsPortB) {
    auto& ioPorts = processor_.getIOPorts();
    ioPorts.port<IOPorts::PortID::A>().set(pinsPortA);
    ioPorts.port<IOPorts::PortID::B>().set(pinsPortB);
    updateGUI({});
}

void Controller::colorCommandFirstCommand() {
    mainWindow_.colorCommand(processor_.getCommandMemory().addressToLineNumber(0));
}


/*
 *
 */
bool Controller::pipeBreakpoints(size_t lineNumber, bool breakpoint) {
    uint16_t address = processor_.getCommandMemory().lineNumberToAddress(lineNumber);
    if (address != static_cast<uint16_t>(-1)) {
        processor_.getCommandMemory().setBreakpoint(address, breakpoint);
        return true;
    }
    return false;
}

size_t Controller::pipeLineCount() {
    return processor_.getCommandMemory().getLineCount();
}

QList<QString> Controller::getCommandStrings() {
    QList<QString> listOfCommand;
    for (int var = 0; var < processor_.getCommandMemory().getLineCount(); var++) {
        listOfCommand << QString::fromStdString(processor_.getCommandMemory().getLine(var));
    }
    return listOfCommand;
}

void Controller::setFrequency(int64_t frequency) {
    if (frequency) {
        processor_.getControlUnit().setFrequency(frequency);
    }
    updateGUI({});
}

void Controller::setSimulationSpeed(int simulationSpeedLevel) {
    if (simulationSpeedLevel == simulationSpeedPeriodToLevel(mainLoop_.getCycleTime())) {
        return;
    }
    if (simulationSpeedLevel < SIM_SPEED_LEVEL_MIN) {
        return;
    } else if (simulationSpeedLevel > SIM_SPEED_LEVEL_MAX) {
        return;
    }
    mainLoop_.setCycleTime(simulationSpeedLevelToPeriod(simulationSpeedLevel));
    updateGUI({});
}

void Controller::resetSimulationSpeed() {
    mainLoop_.setCycleTime(std::max(processor_.getControlUnit().getInstructionTime(), SIM_SPEED_PERIOD_MIN));
    updateGUI({});
}

std::chrono::nanoseconds Controller::simulationSpeedLevelToPeriod(int level) {
    long double exp = static_cast<long double>(SIM_SPEED_LEVEL_MAX - level) / 10;
    return std::chrono::nanoseconds(static_cast<std::chrono::nanoseconds::rep>(roundl(SIM_SPEED_PERIOD_MIN.count() * powl(10, exp))));
}

int Controller::simulationSpeedPeriodToLevel(std::chrono::nanoseconds period) {
    return static_cast<int>(roundl(SIM_SPEED_LEVEL_MAX - 10 * log10l(static_cast<long double>(period.count()) / SIM_SPEED_PERIOD_MIN.count())));
}

void Controller::toggleWatchdog(){
    auto& commandMemory = processor_.getCommandMemory();
    setBitTo<CommandMemory::CONFIG_BITS::WDTE>(commandMemory.getConfig(), !getBit<CommandMemory::CONFIG_BITS::WDTE>(commandMemory.getConfig()));
    updateGUI({});
}

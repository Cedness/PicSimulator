//
// Created by Ced on 06/05/2022.
//

#include "MainLoop.h"

/*
 * Contructor of Mainloop to allow frequent signals
 * @param connection to the controller in order to simulate a clock
 */
MainLoop::MainLoop(Controller& controller)
        : controller_(controller), cycleTime_(controller_.processor_.getControlUnit().getInstructionTime()) {
    timer_->setInterval(0);
    QObject::connect(timer_.get(), &QTimer::timeout, this, &MainLoop::loop);
}

/*
 * starts the programm timer
 */
void MainLoop::start() {
    timer_->start();
}

/*
 * runs the timer for one tick if in stepping-mode
 */
void MainLoop::step() {
    if (running_) {
        return;
    }
    controller_.doCycle({});
    cycleIs_++;
    controller_.updateGUI({});
}

/*
 * resets the timer and changes the Mode the cycles are called
 * @param Mode how to call the cycles
 */
void MainLoop::setRunning(bool running) {
    if (running == running_) {
        return;
    }
    running_ = running;
    if (running_) {
        resetTiming();
    } else {
        controller_.updateGUI({});
    }
}

/*
 * Returns FPS of Display screen
 * @return count of refreschmentrate from screen
 */
int64_t MainLoop::getFramesPerSecond() {
    return periodToFrequency(frameTime_);
}

/*
 * update the possible display speed of screen
 * @param count of refreschmentrate for screen
 */
void MainLoop::setFramesPerSecond(int64_t framesPerSecond) {
    setFrameTime(frequencyToPeriod(framesPerSecond));
}

chr::nanoseconds MainLoop::getFrameTime() {
    return frameTime_;
}

/*
 * Update the time for which a frame stays on screen
 * @param time a frame shloud stay
 */
void MainLoop::setFrameTime(chr::nanoseconds frameTime) {
    frameTime_ = frameTime;
    resetTiming();
}

/*
 * Return the frequentcy of clock ticks
 * @return frequentcy of clock ticks
 */
int64_t MainLoop::getCyclesPerSecond() {
    return periodToFrequency(cycleTime_);
}

/*
 * Update the frequentcy of clock ticks
 * @param frequentcy of clock ticks
 */
void MainLoop::setCyclesPerSecond(int64_t cyclesPerSecond) {
    setCycleTime(frequencyToPeriod(cyclesPerSecond));
}

chr::nanoseconds MainLoop::getCycleTime() {
    return cycleTime_;
}

/*
 * Update the time a clock tick needs
 * @param time how lang a clock tick needs
 */
void MainLoop::setCycleTime(chr::nanoseconds cycleTime) {
    cycleTime_ = cycleTime;
    resetTiming();
}

/*
 * Returns the current time in a high resulution
 * @return pointer to the time stamp when it was execution
 */
chr::time_point<chr::high_resolution_clock> MainLoop::now() {
    return chr::high_resolution_clock::now();
}

/*
 * Calcualte a uniform frequency
 * @param number events in one second
 * @return time in ns for one events
 */
chr::nanoseconds MainLoop::frequencyToPeriod(int64_t frequency) {
    if (frequency <= 0) {
        frequency = 1;
    }
    return SECOND / frequency;
}

/*
 * Calcualte a uniform frequency
 * @param time in ns for one events
 * @return number events in one second
 */
int64_t MainLoop::periodToFrequency(chr::nanoseconds period) {
    return static_cast<int64_t>(SECOND / period);
}

/*
 * Reset the internal program clock to zero relative to current time
 */
void MainLoop::resetTiming() {
    startTime_ = now();
    lastFrameShould_ = startTime_;
    lastFrameIs_ = startTime_;
    droppedFrames_ = 0;
    cycleShould_ = 0;
    cycleIs_ = 0;
    droppedCycles_ = 0;
}

/*
 * Calculate the number of FPS
 * @return number of FPS
 */
int64_t MainLoop::getMeasuredFramesPerSecond() {
    auto lastFrameTime = lastFrameIs_ - lastFrameShould_ + frameTime_;
    return static_cast<int64_t>(round(static_cast<double>(SECOND.count()) / static_cast<double>(lastFrameTime.count())));
}

#if false
#define LOOP_DEBUG(msg) COUT(msg)
#else
#define LOOP_DEBUG(msg)
#endif
/*
 * deturmen life of window, decide behavior with running, optimice cpu usage, update gui after frames
 */
void MainLoop::loop() {
    LOOP_DEBUG("frame " << frame_ << " at " << chr::duration_cast<chr::milliseconds>(now().time_since_epoch()).count() << "\n");
    if (frame_ == 1) {
        startTime_ = now();
        lastFrameShould_ = startTime_;
        lastFrameIs_ = startTime_;
    }
    auto droppedFrames = (lastFrameIs_ - lastFrameShould_) / frameTime_;
    if (droppedFrames > 0) {
        droppedFrames_ += droppedFrames;
        lastFrameShould_ += droppedFrames * frameTime_;
        LOOP_DEBUG("dropped " << droppedFrames << " frames\n");
    }
    auto nextFrameShould = lastFrameShould_ + frameTime_;
    bool cyclesToDo = false;
    if (running_) {
        cycleShould_ = (nextFrameShould - startTime_) / cycleTime_;
        cyclesToDo = cycleIs_ < cycleShould_;
        while (cycleIs_ < cycleShould_) {
            controller_.doCycle({});
            cycleIs_++;
            if (!running_) {
                cycleIs_ = cycleShould_;
                break;
            }
            if (now() >= nextFrameShould) {
                break;
            }
        }
        if (auto droppedCycles = cycleShould_ - cycleIs_) {
            droppedCycles_ += droppedCycles;
            cycleIs_ = cycleShould_;
            LOOP_DEBUG("dropped " << droppedCycles << " cycles\n");
        }
    }
    if (now() < nextFrameShould) {
        LOOP_DEBUG(round(static_cast<double>((frameTime_ - (nextFrameShould - now())).count()) / static_cast<double>(frameTime_.count()) * 100.0) << "% frame-time used, sleeping\n");
        std::this_thread::sleep_until(nextFrameShould);
        lastFrameIs_ = nextFrameShould;
    } else {
        lastFrameIs_ = now();
    }
    lastFrameShould_ = nextFrameShould;
    frame_++;
    LOOP_DEBUG("fps: " << getMeasuredFramesPerSecond() << "\n");
    if (running_ && cyclesToDo) {
        controller_.updateGUI({});
    }
}

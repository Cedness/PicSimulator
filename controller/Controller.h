//
// Created by ced on 02.05.22.
//

#ifndef PICSIMULATOR_CONTROLLER_H
#define PICSIMULATOR_CONTROLLER_H


#include "../simulator/Processor.h"
#include "../simulator/Commands.h"
#include "../gui/mainwindow.h"
#include "MainLoop.h"
#include <chrono>


namespace chr = std::chrono;

class Controller {
    class MainLoopKey {
        MainLoopKey() = default;
        friend Controller;
        friend MainLoop;
    };
public:
    Controller(std::vector<std::string_view>& args);
    void doCycle(MainLoopKey);
    void updateGUI(MainLoopKey);

    //For mainwindow to call
    void start();
    void step();
    void stop();
    void softReset();
    void reload();
    void updateMemory(uint8_t adress, int value);
    void updatePins(uint8_t pinsPortA, uint8_t pinsPortB);
    void setFilePath(const std::string& path);
    void clearProgram();
    size_t pipeLineCount();
    void colorCommandFirstCommand();
    bool pipeBreakpoints(size_t lineNumber, bool breakpoint);
    QList<QString> getCommandStrings();
    void setFrequency(int64_t frequency);
    void setSimulationSpeed(int simulationSpeedLevel);
    void resetSimulationSpeed();
    void toggleWatchdog();
private:
    friend MainLoop::MainLoop(Controller& controller);
    constexpr static int SIM_SPEED_LEVEL_MIN = 0;
    constexpr static int SIM_SPEED_LEVEL_MAX = 100;
    constexpr static std::chrono::nanoseconds SIM_SPEED_PERIOD_MIN{ 10 };
    Processor processor_ = Processor(Commands::get());
    MainWindow mainWindow_ = MainWindow(*this);
    MainLoop mainLoop_ = MainLoop(*this);
    std::string programPath_;
    std::chrono::nanoseconds simulationSpeedLevelToPeriod(int level);
    int simulationSpeedPeriodToLevel(std::chrono::nanoseconds period);
};


#endif //PICSIMULATOR_CONTROLLER_H

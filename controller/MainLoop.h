//
// Created by Ced on 06/05/2022.
//

#ifndef PICSIMULATOR_MAINLOOP_H
#define PICSIMULATOR_MAINLOOP_H


#include "../utility/utility.h"
#include <QTimer>
#include <QObject>
#include <chrono>
#include <algorithm>


namespace chr = std::chrono;

class Controller;

class MainLoop : public QObject {
public:
    explicit MainLoop(Controller& controller);
    void start();
    void step();
    void setRunning(bool running);
    int64_t getFramesPerSecond();
    void setFramesPerSecond(int64_t framesPerSecond);
    chr::nanoseconds getFrameTime();
    void setFrameTime(chr::nanoseconds frameTime);
    int64_t getCyclesPerSecond();
    void setCyclesPerSecond(int64_t cyclesPerSecond);
    chr::nanoseconds getCycleTime();
    void setCycleTime(chr::nanoseconds cycleTime);
private:
    constexpr static chr::nanoseconds SECOND = chr::seconds(1);
    constexpr static int64_t MIN_FPS = 30;

    Controller& controller_;
    std::unique_ptr<QTimer> timer_ = std::make_unique<QTimer>();
    bool running_ = false;
    chr::nanoseconds frameTime_ = frequencyToPeriod(std::max(static_cast<int64_t>(getMonitorRefreshRate()), MIN_FPS));
    chr::nanoseconds cycleTime_;
    chr::time_point<chr::high_resolution_clock> startTime_ = now();
    int64_t frame_ = 0;
    chr::time_point<chr::high_resolution_clock> lastFrameShould_ = startTime_;
    chr::time_point<chr::high_resolution_clock> lastFrameIs_ = startTime_;
    int64_t droppedFrames_ = 0;
    int64_t cycleIs_ = 0;
    int64_t cycleShould_ = 0;
    int64_t droppedCycles_ = 0;

    static chr::time_point<chr::high_resolution_clock> now();
    static chr::nanoseconds frequencyToPeriod(int64_t frequency);
    static int64_t periodToFrequency(chr::nanoseconds period);
    void resetTiming();
    int64_t getMeasuredFramesPerSecond();
private slots:
    void loop();
};


#include "Controller.h"


#endif //PICSIMULATOR_MAINLOOP_H

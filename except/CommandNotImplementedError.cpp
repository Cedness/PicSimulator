//
// Created by Ced on 27/04/2022.
//


#include "CommandNotImplementedError.h"
#include "../utility/utility.h"


CommandNotImplementedError::CommandNotImplementedError(std::string& what_)
        : std::runtime_error(what_) {}

CommandNotImplementedError::CommandNotImplementedError(std::string&& what_)
        : std::runtime_error(std::move(what_)) {}

CommandNotImplementedError::CommandNotImplementedError(uint16_t command)
        : CommandNotImplementedError(F() << "Command not implemented: " << toHex16(command)) {}

//
// Created by Ced on 27/04/2022.
//

#ifndef PICSIMULATOR_COMMANDNOTIMPLEMENTEDERROR_H
#define PICSIMULATOR_COMMANDNOTIMPLEMENTEDERROR_H


#include <stdexcept>


class CommandNotImplementedError : public std::runtime_error {
public:
    CommandNotImplementedError(std::string& what_);
    CommandNotImplementedError(std::string&& what_);
    CommandNotImplementedError(uint16_t command);
    virtual ~CommandNotImplementedError() = default;
};


#endif //PICSIMULATOR_COMMANDNOTIMPLEMENTEDERROR_H

#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <QMainWindow>

#include <QtWidgets>
#include <QPushButton>
#include <QHBoxLayout>
#include <vector>
#include <array>


class Controller;

class MainWindow : public QMainWindow {
Q_OBJECT
public:
    explicit MainWindow(Controller& controller, QWidget* parent = nullptr);
    ~MainWindow() override = default;

    void setW(int value);
    void setPCL(int value);
    void setPcLatch(int value);
    void setPC(int value);
    void setSTPTR(int value);
    void setStatus(int value);
    void setFSR(int value);
    void setTimer0(int time);
    void setWDT(int time);
    void setPS(int time);
    void setCurrentCommend(const QString& command);
    void setStack(int address, int value);
    void setPins(int port, int value);
    void setPinToInput(int port, int value);
    void setClock(int64_t time);
    void setSimSpeedValue(int simSpeed);
    void setSimSpeedSlider(int level);
    void setMemory(int row, int column);
    void setWatchDog(bool wd);
    void colorCommand(int pc);
    void setRowBackground(int pc, bool yellow);
    int getPins(int port);
    long getFrequency();
    void setFrequency(long frequency);
    int getMemory(int address);
    void loadCommands(const QList<QString>& commands);
    void setLocked(bool locked);
private:
    int lastPc = 0;

    bool watchdogOn_ = false;
    bool locked_ = false;

    Controller& controller_;
    QString DecToHex(int);
    std::vector<QString> getBitVector(int);

    QMenuBar* menuBar_;
    QMenu* menu_;
    QAction* actionOpen_;
    QAction* actionExit_;
    QGridLayout* mainLayout_;
    QWidget* leftSectionWidget_;
    QGridLayout* leftSectionLayout_;
    QWidget* seperateMemoryWidget_;
    QHBoxLayout* seperateMemoryLayout_;
    QGroupBox* memoryBox_;
    QGridLayout* memoryLayout_;
    QTableWidget* memoryTableWidget_;
    QWidget* upperSectionWidget_;
    QHBoxLayout* upperSectionLayout_;
    QGroupBox* infoBox_;
    QGridLayout* infoLayout_;
    std::array<QLabel*, 10> infoLabels_;
    std::array<QLineEdit*, 10> infoValues_;
    QWidget* upperCenterWidget_;
    QVBoxLayout* upperCenterLayout_;
    QGroupBox* statusBox_;
    QGridLayout* statusLayout_;
    std::array<QLabel*, 9> statusLabels_;
    std::array<QLineEdit*, 8> statusValues_;
    QGroupBox* currentCommandBox_;
    QHBoxLayout* currentCommandLayout_;
    QLineEdit* currentCommandLabel_;
    QGroupBox* pinBox_;
    QGridLayout* pinLayout_;
    QLabel* portATitel_;
    QWidget* portAWidget_;
    QGridLayout* portALayout_;
    QLabel* portBTitel_;
    QWidget* portBWidget_;
    QGridLayout* portBLayout_;
    std::array<QWidget*, 16> pinButtonWidgets_;
    std::array<QRadioButton*, 16> pinRadioButtons_;
    std::array<QLabel*, 16> pinLabels_;
    QGroupBox* stackBox_;
    QVBoxLayout* stackLayout_;
    std::array<QLabel*, 8> stackLabels_;
    QWidget* lowerSectionWidget_;
    QGridLayout* lowerSectionLayout_;
    QWidget* controlSectionWidget_;
    QGridLayout* controlSectionLayout_;
    QGroupBox* clockBox_;
    QVBoxLayout* clockLayout_;
    QLabel* clockLabel_;
    QGroupBox* frequencykBox_;
    QVBoxLayout* frequencyLayout_;
    QLineEdit* frequencValue_;
    QLabel* validFrequenceLabel_;
    QGroupBox* simSpeedBox_;
    QVBoxLayout* simSpeedLayout_;
    QLabel* simSpeedLabel_;
    QSlider* simSpeedSlider_;
    QPushButton* simSpeedButton_;
    QGroupBox* controlBox_;
    QVBoxLayout* controlLayout_;
    std::array<QPushButton*, 8> controlButtons_;
    QGroupBox* commandBox_;
    QGridLayout* commandLayout_;
    QTableWidget* commandTableWidget_;
    std::unique_ptr<QWidget> mainWidget_;
    std::array<QVBoxLayout*, 16> radioBoxLayouts_;

    std::vector<QTableWidgetItem*> code_;
    std::vector<QWidget*> breakpointWidget_;
    std::vector<QHBoxLayout*> breakpointLayout_;
    std::vector<QCheckBox*> breakCheckBox_;

private slots:
    void openFileAction();
    void closeWindow();

    void sendPinStatus();
    void frequencyChanged();
    void simSpeedChanged(int simSpeedLevel);
    void changeMemory(int row, int column);
    void changeBreakpoint(bool state);
    void setbackSlider();

    void clickClearBreaks();
    void clickClearCode();
    void clickReload();
    void clickReset();
    void clickStart();
    void clickStep();
    void clickStop();
    void clickToggleWD();
};


#include "../controller/Controller.h"


#endif // MAINWINDOW_H

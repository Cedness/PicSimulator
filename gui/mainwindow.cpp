#include "mainwindow.h"
#include <memory>

//--------------------Funktions for Variables------------------------
//-------------------------------------------------------------------
#include <iostream>


using namespace std;

/*
 * function to convert decimal to hexadecimal
 * @param decimal number
 * @retrun hex presentation
 */
QString MainWindow::DecToHex(int decNumber) {
    int remainder, j, i = 0;
    char Hexa_Num[50];
    while (decNumber != 0) {
        remainder = decNumber % 16;
        if (remainder < 10) {
            Hexa_Num[i] = remainder + 48;
            i++;
        } else {
            Hexa_Num[i] = remainder + 55;
            i++;
        }
        decNumber = decNumber / 16;
    }
    QString number;
    for (j = (i - 1); j >= 0; j--) {
        number.append(Hexa_Num[j]);
        if (j == 0) {
            number.append('h');
            return number;
        }
    }
    return "0h";
}

/*
 * function to get a bits of a vlaue in vector
 * @param decimal value to be represented
 * @return vector representing binary value
 */
vector<QString> MainWindow::getBitVector(int value) {
    vector<QString> bits;
    while (value != 0) {
        if (value % 2) {
            bits.emplace_back("1");
        } else {
            bits.emplace_back("0");
        }
        value = value / 2;
    }
    while (bits.size() < 8) {
        bits.emplace_back("0");
    }
    return bits;
}
//-------------------------------------------------------------------
//-------------------------------------------------------------------

//------------------Constructor for MainWindow-----------------------
/*
 * Constructor of gui with creation and composition of gui elements
 */
MainWindow::MainWindow(Controller& controller, QWidget* parent)
        : controller_(controller), QMainWindow(parent) {
//-----------------------------MenuBar-------------------------------
    menuBar_ = new QMenuBar(this);
    setMenuBar(menuBar_);
    menu_ = menuBar_->addMenu("&File");
    // Open
    actionOpen_ = new QAction("&Open", this);
    connect(actionOpen_, &QAction::triggered, this, &MainWindow::openFileAction);
    menu_->addAction(actionOpen_);
    // Exit
    actionExit_ = new QAction("&Exit", this);
    connect(actionExit_, &QAction::triggered, this, &MainWindow::closeWindow);
    menu_->addAction(actionExit_);

    //-------------------------------Memory------------------------------
    mainLayout_ = new QGridLayout();
    leftSectionWidget_ = new QWidget();
    leftSectionLayout_ = new QGridLayout();

    seperateMemoryWidget_ = new QWidget();
    seperateMemoryLayout_ = new QHBoxLayout();

    memoryBox_ = new QGroupBox("Memory:");
    memoryLayout_ = new QGridLayout();
    memoryTableWidget_ = new QTableWidget();
    memoryTableWidget_->setRowCount(32);
    memoryTableWidget_->setColumnCount(8);

    QStringList headerHorizontal;
    for (int var = 0; var < 9; var++) {
        QString number = QString("+%1").arg(var);
        headerHorizontal << number;

        memoryTableWidget_->setColumnWidth(var, 1);
    }
    memoryTableWidget_->setHorizontalHeaderLabels(headerHorizontal);

    QStringList headerVertical;
    for (int var = 0; var < 32; var++) {
        QString number = QString("%1").arg(DecToHex((var * 8)));
        headerVertical << number;
    }
    memoryTableWidget_->setVerticalHeaderLabels(headerVertical);

    QObject::connect(memoryTableWidget_, SIGNAL(cellChanged(int, int)), this, SLOT(changeMemory(int, int)));


    //--------------------------------InfoBox----------------------------
    upperSectionWidget_ = new QWidget();
    upperSectionLayout_ = new QHBoxLayout();

    infoBox_ = new QGroupBox("Info:");
    infoLayout_ = new QGridLayout();
    for (int var = 0; var < infoLabels_.size(); var++) {
        infoLabels_.at(var) = new QLabel();
        infoValues_.at(var) = new QLineEdit();
        infoValues_[var]->setAlignment(Qt::AlignRight);
        infoValues_[var]->setText("0");
        infoValues_[var]->setReadOnly(true);
    }
    infoLabels_[0]->setText("W:");
    infoLabels_[1]->setText("PCL:");
    infoLabels_[2]->setText("PCLatch:");
    infoLabels_[3]->setText("PC:");
    infoLabels_[4]->setText("STPTR:");
    infoLabels_[5]->setText("Status:");
    infoLabels_[6]->setText("FSR:");
    infoLabels_[7]->setText("Timer0:");
    infoLabels_[8]->setText("WDT:");
    infoLabels_[9]->setText("PS:");

    //-------------------------------StatusBox---------------------------
    upperCenterWidget_ = new QWidget();
    upperCenterLayout_ = new QVBoxLayout();

    statusBox_ = new QGroupBox("Status:");
    statusLayout_ = new QGridLayout();
    for (int var = 0; var < 9; var++) {
        statusLabels_.at(var) = new QLabel();
        statusLabels_[var]->setAlignment(Qt::AlignCenter);
    }
    for (int var = 0; var < 8; var++) {
        statusValues_.at(var) = new QLineEdit();
        statusValues_[var]->setText("0");
        statusValues_[var]->setAlignment(Qt::AlignCenter);
        statusValues_[var]->setReadOnly(true);
    }
    statusLabels_[0]->setText("Status:");
    statusLabels_[1]->setText("IRP");
    statusLabels_[2]->setText("RP1");
    statusLabels_[3]->setText("RP0");
    statusLabels_[4]->setText("!TO");
    statusLabels_[5]->setText("!PD");
    statusLabels_[6]->setText("Z");
    statusLabels_[7]->setText("DC");
    statusLabels_[8]->setText("C");
    statusLayout_->setAlignment(Qt::AlignCenter);

    //--------------------------current command--------------------------
    currentCommandBox_ = new QGroupBox("Current command:");
    currentCommandLayout_ = new QHBoxLayout();
    currentCommandLabel_ = new QLineEdit();
    currentCommandLabel_->setReadOnly(true);

    //--------------------------------PinBox-----------------------------
    pinBox_ = new QGroupBox("Ports:");
    pinLayout_ = new QGridLayout();

    portATitel_ = new QLabel("Port A:");
    portAWidget_ = new QWidget();
    portALayout_ = new QGridLayout();

    portBTitel_ = new QLabel("Port B:");
    portBWidget_ = new QWidget();
    portBLayout_ = new QGridLayout();
    for (int var = 0; var < 16; var++) {
        pinButtonWidgets_.at(var) = new QWidget();
        pinRadioButtons_.at(var) = new QRadioButton();
        QObject::connect(pinRadioButtons_[var], SIGNAL(toggled(bool)), this, SLOT(sendPinStatus()));
        pinLabels_.at(var) = new QLabel();
        pinLabels_[var]->setText("O");
        pinLabels_[var]->setAlignment(Qt::AlignCenter);
    }

    //-------------------------------StackBox----------------------------
    stackBox_ = new QGroupBox("Stack:");
    stackLayout_ = new QVBoxLayout();
    for (int var = 0; var < 8; var++) {
        stackLabels_.at(var) = new QLabel();
        stackLabels_[var]->setText("0");
        stackLabels_[var]->setAlignment(Qt::AlignCenter);
    }

    //-------------------------------ClockBox-----------------------------
    lowerSectionWidget_ = new QWidget();
    lowerSectionLayout_ = new QGridLayout();

    controlSectionWidget_ = new QWidget();
    controlSectionLayout_ = new QGridLayout();

    clockBox_ = new QGroupBox("Clock:");
    clockLayout_ = new QVBoxLayout();
    clockLabel_ = new QLabel();
    clockLabel_->setText("0 µs");
    clockLabel_->setAlignment(Qt::AlignRight);
    frequencykBox_ = new QGroupBox("frequency (Hz):");
    frequencyLayout_ = new QVBoxLayout();
    frequencValue_ = new QLineEdit();
    frequencValue_->setText("40000");
    QObject::connect(frequencValue_, SIGNAL(editingFinished()), this, SLOT(frequencyChanged()));
    frequencValue_->setAlignment(Qt::AlignCenter);
    validFrequenceLabel_ = new QLabel();
    validFrequenceLabel_->setText("is valid:");
    validFrequenceLabel_->setAlignment(Qt::AlignLeft);

    //-------------------Simmulations Speed controll---------------------
    simSpeedBox_ = new QGroupBox("Simulation speed:");
    simSpeedLayout_ = new QVBoxLayout();
    simSpeedLabel_ = new QLabel();
    simSpeedLabel_->setText("Speed: -");
    simSpeedLabel_->setAlignment(Qt::AlignRight);
    simSpeedSlider_ = new QSlider(Qt::Horizontal);
    simSpeedSlider_->setMinimum(0);
    simSpeedSlider_->setMaximum(100);
    simSpeedSlider_->setSliderPosition(20);
    QObject::connect(simSpeedSlider_, SIGNAL(valueChanged(int)), this, SLOT(simSpeedChanged(int)));
    simSpeedButton_ = new QPushButton();
    simSpeedButton_->setText("reset to pic-speed");
    QObject::connect(simSpeedButton_, SIGNAL(clicked()), this, SLOT(setbackSlider()));

    //-----------------------Make Control Buttons------------------------
    controlBox_ = new QGroupBox("Controls:");
    controlLayout_ = new QVBoxLayout();
    for (int var = 0; var < 8; var++) {
        controlButtons_.at(var) = new QPushButton();
    }
    controlButtons_[0]->setText("clear break");
    controlButtons_[1]->setText("clear code");
    controlButtons_[2]->setText("power cycle");
    controlButtons_[3]->setText("reset (mclr)");
    controlButtons_[4]->setText("start");
    controlButtons_[5]->setText("step");
    controlButtons_[6]->setText("stop");
    controlButtons_[7]->setText("turn on WDT");

    QObject::connect(controlButtons_[0], SIGNAL(clicked()), this, SLOT(clickClearBreaks()));
    QObject::connect(controlButtons_[1], SIGNAL(clicked()), this, SLOT(clickClearCode()));
    QObject::connect(controlButtons_[2], SIGNAL(clicked()), this, SLOT(clickReload()));
    QObject::connect(controlButtons_[3], SIGNAL(clicked()), this, SLOT(clickReset()));
    QObject::connect(controlButtons_[4], SIGNAL(clicked()), this, SLOT(clickStart()));
    QObject::connect(controlButtons_[5], SIGNAL(clicked()), this, SLOT(clickStep()));
    QObject::connect(controlButtons_[6], SIGNAL(clicked()), this, SLOT(clickStop()));
    QObject::connect(controlButtons_[7], SIGNAL(clicked()), this, SLOT(clickToggleWD()));

    //------------------------Table to show Commands---------------------
    commandBox_ = new QGroupBox("Commands:");
    commandLayout_ = new QGridLayout();
    commandTableWidget_ = new QTableWidget();

    commandTableWidget_->setRowCount(20);
    commandTableWidget_->setColumnCount(2);
    commandTableWidget_->setColumnWidth(1, 840);

    QStringList commandHeaderHorizontal;
    commandHeaderHorizontal << "break" << "command code";
    commandTableWidget_->setHorizontalHeaderLabels(commandHeaderHorizontal);

    //-----------------------------Add to Widget-------------------------
    //-------------------------------------------------------------------
    mainWidget_ = std::make_unique<QWidget>();
    mainWidget_->setLayout(mainLayout_);

    mainLayout_->addWidget(leftSectionWidget_, 0, 0);
    mainLayout_->addWidget(seperateMemoryWidget_, 0, 1);
    mainLayout_->setColumnStretch(0, 20);

    seperateMemoryWidget_->setLayout(seperateMemoryLayout_);
    seperateMemoryLayout_->addWidget(memoryBox_);

    memoryBox_->setLayout(memoryLayout_);
    memoryLayout_->addWidget(memoryTableWidget_);

    leftSectionWidget_->setLayout(leftSectionLayout_);
    leftSectionLayout_->addWidget(upperSectionWidget_, 0, 0);
    leftSectionLayout_->setRowStretch(1, 12);
    upperSectionWidget_->setLayout(upperSectionLayout_);

    upperSectionLayout_->addWidget(infoBox_);
    infoBox_->setLayout(infoLayout_);
    for (int var = 0; var < 10; var++) {
        if (var < 5) {
            infoLayout_->addWidget(infoLabels_[var], var, 0);
            infoLayout_->addWidget(infoValues_[var], var, 1);
        } else {
            infoLayout_->addWidget(infoLabels_[var], var % 5, 3);
            infoLayout_->addWidget(infoValues_[var], var % 5, 4);
        }
    }


    upperSectionLayout_->addWidget(upperCenterWidget_);
    upperCenterWidget_->setLayout(upperCenterLayout_);
    upperCenterLayout_->addWidget(statusBox_);
    statusBox_->setLayout(statusLayout_);
    statusLayout_->addWidget(statusLabels_[0], 0, 0);
    for (int var = 1; var <= 8; var++) {
        statusLayout_->addWidget(statusLabels_[var], 0, var);
        statusLayout_->addWidget(statusValues_[var - 1], 1, var);
    }
    upperCenterLayout_->addWidget(currentCommandBox_);
    currentCommandBox_->setLayout(currentCommandLayout_);
    currentCommandLayout_->addWidget(currentCommandLabel_);


    upperSectionLayout_->addWidget(pinBox_);
    pinBox_->setLayout(pinLayout_);

    pinLayout_->addWidget(portATitel_);
    pinLayout_->addWidget(portAWidget_);
    portAWidget_->setLayout(portALayout_);
    pinLayout_->addWidget(portBTitel_);
    pinLayout_->addWidget(portBWidget_);
    portBWidget_->setLayout(portBLayout_);
    for (int var = 0; var < 8; var++) {
        radioBoxLayouts_.at(var) = new QVBoxLayout();
        portALayout_->addWidget(pinButtonWidgets_[var], 0, 7 - var);
        pinButtonWidgets_[var]->setLayout(radioBoxLayouts_[var]);
        radioBoxLayouts_[var]->addWidget(pinRadioButtons_[var]);

        portALayout_->addWidget(pinLabels_[var], 1, var);
    }
    for (int var = 8; var < 16; var++) {
        radioBoxLayouts_.at(var) = new QVBoxLayout();
        portBLayout_->addWidget(pinButtonWidgets_[var], 0, 7 - var % 8);
        pinButtonWidgets_[var]->setLayout(radioBoxLayouts_[var]);
        radioBoxLayouts_[var]->addWidget(pinRadioButtons_[var]);

        portBLayout_->addWidget(pinLabels_[var], 1, var % 8);
    }

    upperSectionLayout_->addWidget(stackBox_);
    stackBox_->setLayout(stackLayout_);
    for (int var = 0; var < 8; var++) {
        stackLayout_->addWidget(stackLabels_[var]);
    }

    leftSectionLayout_->addWidget(lowerSectionWidget_, 1, 0);
    lowerSectionWidget_->setLayout(lowerSectionLayout_);
    lowerSectionLayout_->setColumnStretch(1, 15);

    lowerSectionLayout_->addWidget(controlSectionWidget_);
    controlSectionWidget_->setLayout(controlSectionLayout_);
    controlSectionLayout_->addWidget(clockBox_);
    clockBox_->setLayout(clockLayout_);
    clockLayout_->addWidget(clockLabel_);
    controlSectionLayout_->addWidget(frequencykBox_);
    frequencykBox_->setLayout(frequencyLayout_);
    frequencyLayout_->addWidget(frequencValue_);
    frequencyLayout_->addWidget(validFrequenceLabel_);
    controlSectionLayout_->addWidget(simSpeedBox_);
    simSpeedBox_->setLayout(simSpeedLayout_);
    simSpeedLayout_->addWidget(simSpeedLabel_);
    simSpeedLayout_->addWidget(simSpeedSlider_);
    simSpeedLayout_->addWidget(simSpeedButton_);

    controlSectionLayout_->addWidget(controlBox_);
    controlSectionLayout_->setRowStretch(1, 20);
    controlBox_->setLayout(controlLayout_);
    for (int var = 0; var < 8; var++) {
        controlLayout_->addWidget(controlButtons_[var]);
    }

    lowerSectionLayout_->addWidget(commandBox_, 0, 1);
    commandBox_->setLayout(commandLayout_);
    commandLayout_->addWidget(commandTableWidget_);

    mainLayout_->setMenuBar(menuBar_);
    mainWidget_->show();
}
//-------------------------------------------------------------------
//-------------------------------------------------------------------

//-------------------------------------------------------------------
//--------------------Set Methodes-----------------------
void MainWindow::setW(int value) {
    infoValues_[0]->setText(DecToHex(value));
}

void MainWindow::setPCL(int value) {
    infoValues_[1]->setText(DecToHex(value));
}

void MainWindow::setPcLatch(int value) {
    infoValues_[2]->setText(DecToHex(value));
}

void MainWindow::setPC(int value) {
    infoValues_[3]->setText(DecToHex(value));
}

void MainWindow::setSTPTR(int value) {
    infoValues_[4]->setText(DecToHex(value));
}

void MainWindow::setStatus(int value) {
    infoValues_[5]->setText(DecToHex(value));
    vector<QString> statusBits = getBitVector(value);
    for (int var = 0; var < 8; var++) {
        statusValues_[7 - var]->setText(statusBits[var]);
    }
}

void MainWindow::setFSR(int value) {
    infoValues_[6]->setText(DecToHex(value));
}

void MainWindow::setTimer0(int value) {
    infoValues_[7]->setText(DecToHex(value));
}

void MainWindow::setWDT(int value) {
    infoValues_[8]->setText(DecToHex(value));
}

void MainWindow::setPS(int value) {
    infoValues_[9]->setText(DecToHex(value));
}

void MainWindow::setCurrentCommend(const QString& command) {
    currentCommandLabel_->setText(" " + command);
}

void MainWindow::setStack(int address, int value) {
    stackLabels_[7 - address]->setText(DecToHex(value));
}

void MainWindow::setPins(int port, int value) {
    vector<QString> pinBits = getBitVector(value);
    if (port == 0) {
        for (int var = 0; var < 8; var++) {
            if (pinBits[var] == "1") {
                pinRadioButtons_[var]->setChecked(true);
            } else {
                pinRadioButtons_[var]->setChecked(false);
            }
        }
    } else {
        for (int var = 0; var < 8; var++) {
            if (pinBits[var] == "1") {
                pinRadioButtons_[var + 8]->setChecked(true);
            } else {
                pinRadioButtons_[var + 8]->setChecked(false);
            }
        }
    }
}

void MainWindow::setPinToInput(int port, int pin) {
    int pinType;
    for (int pinPosition = 0; pinPosition < 8; pinPosition++) {
        pinType = pin % 2;
        if (pinType) {
            pinLabels_.at(7 - pinPosition + (8 * port))->setText("I");
        } else {
            pinLabels_.at(7 - pinPosition + (8 * port))->setText("O");
        }
        pin = pin / 2;
    }
}

void MainWindow::setClock(int64_t time) {
    QString controlerTime = QString("%1 µs").arg(time);
    clockLabel_->setText(controlerTime);
}

void MainWindow::setSimSpeedValue(int simSpeed) {
    QString speedString = QString("Speed: %1").arg(simSpeed);
    simSpeedLabel_->setText(speedString);
}

void MainWindow::setSimSpeedSlider(int level) {
    simSpeedSlider_->setSliderPosition(level);
}

void MainWindow::setMemory(int adress, int value) {
    QString memoryValue = QString::number(value, 16);

    auto* item = new QTableWidgetItem;
    item->setTextAlignment(Qt::AlignCenter);
    item->setText(memoryValue);
    memoryTableWidget_->setItem(adress / 8, adress % 8, item);
}

void MainWindow::setWatchDog(bool wd) {
    if (wd) {
        controlButtons_[7]->setText("turn off WD");
    }
    else{
        controlButtons_[7]->setText("turn on WD");
    }
}

void MainWindow::colorCommand(int pc) {
    setRowBackground(lastPc, false);
    setRowBackground(pc, true);
    lastPc = pc;
    commandTableWidget_->scrollToItem(commandTableWidget_->item(pc, 1), QAbstractItemView::EnsureVisible);
}

void MainWindow::setRowBackground(int pc, bool placeColor) {
    if (commandTableWidget_->item(pc, 1) != nullptr) {
        if (placeColor) {
            breakpointWidget_[pc]->setStyleSheet("background-color:rgb(0,155,0);");
            commandTableWidget_->item(pc, 1)->setBackground(QColor(0,155,0));
        } else {
            breakpointWidget_[pc]->setStyleSheet("background-color:;");
            commandTableWidget_->item(pc, 1)->setBackground(QColor(0, 0, 0, 0));
        }
    }
}

int MainWindow::getPins(int port) {
    int portValue = 0;
    if (port == 0) {
        for (int var = 0; var < 8; var++) {
            portValue = (portValue << 1) + pinRadioButtons_[7 - var]->isChecked();
        }
        return portValue;
    } else {
        for (int var = 8; var < 16; var++) {
            portValue = (portValue << 1) + pinRadioButtons_[15 - var % 8]->isChecked();
        }
        return portValue;
    }
}


long MainWindow::getFrequency() {
    bool ok;
    long frequency = (frequencValue_->displayText()).toLong(&ok);
    if (!ok || frequency <= 0) {
        validFrequenceLabel_->setText("is valid: no");
        frequency = 0;
    } else {
        validFrequenceLabel_->setText("is valid: yes");
    }
    return frequency;
}

void MainWindow::setFrequency(long frequency) {
    frequencValue_->setText(QString::number(frequency));
}

void MainWindow::setbackSlider() {
    controller_.resetSimulationSpeed();
}

int MainWindow::getMemory(int address) {
    return ((memoryTableWidget_->item(address / 8, address % 8))->text()).toInt();
}

void MainWindow::loadCommands(const QList<QString>& commands) {
    commandTableWidget_->clearContents();

    code_.clear();
    breakpointWidget_.clear();
    breakpointLayout_.clear();
    breakCheckBox_.clear();

    commandTableWidget_->setRowCount(commands.size());
    for (int var = 0; var < commands.size(); var++) {
        code_.push_back(new QTableWidgetItem());
        breakpointWidget_.push_back(new QWidget());
        breakpointLayout_.push_back(new QHBoxLayout(breakpointWidget_[var]));
        breakCheckBox_.push_back(new QCheckBox());
        breakCheckBox_[var]->setAccessibleName(QString("%1").arg(var));
        breakpointLayout_[var]->addWidget(breakCheckBox_[var]);
        breakpointLayout_[var]->setAlignment(Qt::AlignCenter);
        commandTableWidget_->setCellWidget(var, 0, breakpointWidget_[var]);
        QObject::connect(breakCheckBox_[var], SIGNAL(toggled(bool)), this, SLOT(changeBreakpoint(bool))/*,Qt::UniqueConnection*/);
        code_[var]->setText("  " + commands.value(var));
        commandTableWidget_->setItem(var, 1, code_[var]);
    }
}

void MainWindow::setLocked(bool locked) {
    locked_ = locked;
}

void MainWindow::openFileAction() {
    const QString fileName = QFileDialog::getOpenFileName(this, "open File", "C:/Users/betas/Documents/Schule/Abgaben/PicSimulator/docs/Tests");
    controller_.setFilePath(fileName.toStdString());
}

void MainWindow::closeWindow() {
    QApplication::quit();
}

void MainWindow::sendPinStatus() {
    controller_.updatePins(getPins(0), getPins(1));
}

void MainWindow::frequencyChanged() {
    auto frequency = getFrequency();
    controller_.setFrequency(frequency);
    // send to controller(getFrequency())
}

void MainWindow::simSpeedChanged(int simSpeedLevel) {
    if (locked_)
        return;
    controller_.setSimulationSpeed(simSpeedLevel);
}

void MainWindow::changeMemory(int row, int column) {
    if (locked_)
        return;
    bool ok;
    int changedMemoryValue = ((memoryTableWidget_->item(row, column))->text()).toInt(&ok,16);
    if (!ok){
        changedMemoryValue = -1;
    }
    controller_.updateMemory((row * 8) + column, changedMemoryValue);
}

void MainWindow::changeBreakpoint(bool state) {
    QObject* sender = this->sender();
    QCheckBox* breakpoint = qobject_cast<QCheckBox*>(sender);
    size_t nameCheckbox = (breakpoint->accessibleName()).toULongLong();
    if (!controller_.pipeBreakpoints(nameCheckbox, state)) {
        breakpoint->setChecked(false);
    }
}

//--------------------Funktions for Control Buttons------------------
//-------------------------------------------------------------------
void MainWindow::clickClearBreaks() {
    for (int var = 0; var < commandTableWidget_->rowCount(); var++) {
        breakCheckBox_[var]->setCheckState(Qt::Unchecked);
    }
}

void MainWindow::clickClearCode() {
    controller_.clearProgram();
}

void MainWindow::clickReload() {
    controller_.reload();
}

void MainWindow::clickReset() {
    controller_.softReset();
}

void MainWindow::clickStart() {
    controller_.start();
}

void MainWindow::clickStep() {
    controller_.step();
}

void MainWindow::clickStop() {
    controller_.stop();
}

void MainWindow::clickToggleWD() {
        controller_.toggleWatchdog();
}
//-------------------------------------------------------------------
//-------------------------------------------------------------------
